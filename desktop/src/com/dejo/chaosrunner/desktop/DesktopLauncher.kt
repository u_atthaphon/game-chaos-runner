package com.dejo.chaosrunner.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.dejo.chaosrunner.Handler.BillingHandlerInterface
import com.dejo.chaosrunner.ChaosRunner
import com.dejo.chaosrunner.Handler.AndroidHandlerInterface
import com.dejo.chaosrunner.ads.AdmobHandlerInterface
import com.dejo.chaosrunner.configs.*


object DesktopLauncher : AndroidHandlerInterface, AdmobHandlerInterface, BillingHandlerInterface {
    var desktopApplication: DesktopLauncher = DesktopLauncher

    override var moreLife: Int = 0
    override var isRewardMoreLife: Boolean = true

    @JvmStatic fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration()
        config.width = WIDTH.toInt()
        config.height = HEIGHT.toInt()
        LwjglApplication(ChaosRunner(desktopApplication, desktopApplication, desktopApplication), config)
    }

    override fun showAdFooter(show: Boolean) {
        return
    }

    override fun showAdGameOver(startTime: Long, ImpressionTime: Float): Boolean {
        return false
    }

    override fun showAdRewardVdoMoreLife() {
        return
    }

    override fun loadAdRewardVdoMoreLife() {
        return
    }

    override fun isPurchaseRemoveAds(): Boolean {
        return false
    }

    override fun isPurchaseLifeUpTo5(): Boolean {
        return false
    }

    override fun isPurchaseLifeUpTo10(): Boolean {
        return false
    }

    override fun purchaseRemoveAds() {
        return
    }

    override fun purchaseLifeUpTo5() {
        return
    }

    override fun purchaseLifeUpTo10() {
        return
    }

    override fun getRemoveAdsPrice(): String {
        return "not available in desktop"
    }

    override fun isConnectToInternet(): Boolean {
        return true
    }

    override fun toast(text: String) {
        return
    }
}
