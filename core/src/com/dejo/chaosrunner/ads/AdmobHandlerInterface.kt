package com.dejo.chaosrunner.ads

interface AdmobHandlerInterface {
    var moreLife: Int
    var isRewardMoreLife: Boolean

    fun showAdFooter(show: Boolean)
    fun showAdGameOver(startTime: Long, ImpressionTime: Float): Boolean
    fun showAdRewardVdoMoreLife()
    fun loadAdRewardVdoMoreLife()
}