package com.dejo.chaosrunner.configs

import ktx.collections.GdxArray
import ktx.collections.gdxArrayOf

val COMPLETE_MESSAGE: GdxArray<String> = gdxArrayOf(
        "COMPLETE", "COOL MAN", "AWESOME"
)