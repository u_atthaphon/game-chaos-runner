package com.dejo.chaosrunner.configs

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Preferences
import ktx.log.info

enum class PrefsKey {
    CURRENT_LEVEL, MAX_LIFE
}

data class Prefs(
        var prefs: Preferences? = Gdx.app.getPreferences("save_game")
) {
    fun get(key: PrefsKey, defaultValue: Any? = null): Any {
        when (key) {
            PrefsKey.MAX_LIFE -> {
                val value = if (defaultValue == null) 0 else defaultValue as Int
                return prefs!!.getInteger(key.toString(), value)
            }
            PrefsKey.CURRENT_LEVEL -> {
                val value = if (defaultValue == null) 0 else defaultValue as Int
                return prefs!!.getInteger(key.toString(), value)
            }
            else -> {
                info { "No ${key.toString()} in preferencs yet!" }
                return false
            }
        }

    }

    fun put(key: PrefsKey, value: Any) {
        when (value) {
            is String -> prefs!!.putString(key.toString(), value.toString())
            is Int -> prefs!!.putInteger(key.toString(), value)
            is Float -> prefs!!.putFloat(key.toString(), value)
            is Long -> prefs!!.putLong(key.toString(), value)
            is Boolean -> prefs!!.putBoolean(key.toString(), value)
        }
    }

    fun has(key: PrefsKey):Boolean {
        return prefs!!.contains(key.toString())
    }
    fun save() {
        prefs!!.flush()
    }
}