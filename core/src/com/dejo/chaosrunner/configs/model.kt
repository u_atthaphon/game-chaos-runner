package com.dejo.chaosrunner.configs

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import ktx.collections.GdxArray
import ktx.collections.gdxArrayOf

enum class ModelType {
    GROUND, PLAYER, PLAYER_FOOT, OBSTACLE, PLATFORM, COIN, NONE
}

enum class Direction {
    STOP, LEFT, RIGHT
}

data class StartPosition(
        val name: String, val position: Vector2
)

data class Collectible(
        val name: String,
        val textureRegion: TextureRegion,
        var textureRegions: GdxArray<TextureRegion> = GdxArray<TextureRegion>(),
        var animations: Animation<TextureRegion> = Animation<TextureRegion>(1 / 2f, GdxArray<TextureRegion>()),
        var stateTime: Float = 0f,
        val position: Vector2,
        val size: Vector2,
        val floor: Int
)

data class PlayerData(
        var speed: Float = 2.5f,
        var maxSpeed: Float = 8f,
        var jumpVelocity: Float = 8f,
        var direction: Direction = Direction.STOP,
        var position: Vector2 = Vector2(B2D_WIDTH / 2f, B2D_HEIGHT / 2f),
        var width: Float = 32 / PTM,
        var height: Float = 32 / PTM,
        var coins: GdxArray<Collectible> = GdxArray<Collectible>()
)