package com.dejo.chaosrunner.configs

val WIDTH: Float = 414f
val HEIGHT: Float = 736f // 736 / 3 will be 245

val PTM: Float = 32f // pixel to meter for box2d
val UNIT_SCALE: Float = 1 / PTM // pixel to meter for box2d
val B2D_WIDTH: Float = WIDTH / PTM
val B2D_HEIGHT: Float = HEIGHT / PTM
