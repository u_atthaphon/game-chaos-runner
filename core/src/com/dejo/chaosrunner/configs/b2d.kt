package com.dejo.chaosrunner.configs

object B2dStep {
    const val timeStep: Float = 1 / 45f
    const val velocityIterations: Int = 6
    const val positionIterations: Int = 2
}

object B2dCategory {
    const val GROUND: Short = 0x0001
    const val PLAYER: Short = 0x0002
    const val BRICK: Short = 0x0003
    const val PLATFORM: Short = 0x0004
}