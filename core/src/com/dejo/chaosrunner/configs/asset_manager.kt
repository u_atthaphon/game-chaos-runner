package com.dejo.chaosrunner.configs

val ATLAS_IMAGES: String = "data/chaos_runner.atlas"
val TILED_MAP_MENU: String = "data/maps/menu.tmx"
val TILED_MAP_STAGE_MENU: String = "data/maps/stage_menu.tmx"
val MUSIC_THEME: String = "data/music/Mile_High.mp3"
val SOUND_JUMP: String = "data/sounds/jump.wav"
val SOUND_SELECTED: String = "data/sounds/selected.wav"
val SOUND_COLLECTED: String = "data/sounds/collected.wav"
val SOUND_GAME_OVER: String = "data/sounds/game_over.wav"
val SOUND_COMPLETE: String = "data/sounds/complete.wav"
val SOUND_HIT: String = "data/sounds/hit.wav"
val SOUND_READY: String = "data/sounds/ready.wav"
