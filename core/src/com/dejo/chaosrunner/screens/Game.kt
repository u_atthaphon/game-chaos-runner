package com.dejo.chaosrunner.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.World
import com.dejo.chaosrunner.ChaosRunner
import com.dejo.chaosrunner.configs.*
import com.dejo.chaosrunner.events.GameContactListener
import com.dejo.chaosrunner.managers.GameManager
import com.dejo.chaosrunner.managers.StageMode
import com.dejo.chaosrunner.models.GameMap
import com.dejo.chaosrunner.models.Player
import ktx.app.KtxScreen
import ktx.log.info
import com.badlogic.gdx.scenes.scene2d.Stage
import ktx.scene2d.*
import ktx.actors.*
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.Align
import ktx.style.get


class Game(val app: ChaosRunner) : KtxScreen {
    enum class GameStage {
        TUTORIAL, LOAD_MAP, READY, PLAY, PAUSE, GAME_OVER, BACK_CONFIRMATION, LEVEL_COMPLETE
    }

    data class Hud(
            var app: ChaosRunner,
            var game: Game,
            var table: Table = Table(),
            var levelText: String = "LEVEL 1",
            var backToMenuBtn: ImageButton = ImageButton(Scene2DSkin.defaultSkin, "menu"),
            var playSwitcherBtn: ImageButton = ImageButton(Scene2DSkin.defaultSkin, "play_switcher"),
            var soundSwitcherBtn: ImageButton = ImageButton(Scene2DSkin.defaultSkin, "sound_switcher")
    ) {
        init {
            table = table {
                backToMenuBtn = imageButton("menu") {
                    name = "backButton"
                    onClick {
                        app.sounds.playSelected()
                        game.gameStage = GameStage.BACK_CONFIRMATION
                    }
                }.cell(pad = 5f, width = 50f, height = 50f)
                label(text = levelText, style = "yellow") {
                    name = "levelLabel"
                    setFontScale(0.7f)
                }.cell(expandX = true)

                image("player").cell(pad = 5f, padRight = 0f, width = 22f, height = 22f)
                label(text = " 5", style = "yellow") {
                    name = "lifeLabel"
                    setFontScale(0.7f)
                }.cell(pad = 5f, padLeft = 0f, padRight = 15f, align = Align.left)

                playSwitcherBtn = imageButton("play_switcher") {
                    name = "playSwitcher"
                    onClick {
                        app.sounds.playSelected()
                        game.pauseOverlay.isActive = !game.pauseOverlay.isActive

                        when (game.pauseOverlay.isActive) {
                            true -> game.gameStage = GameStage.PAUSE
                            false -> game.gameStage = GameStage.PLAY
                        }
                    }
                }.cell(pad = 5f, width = 50f, height = 50f, align = Align.right)
                soundSwitcherBtn = imageButton("sound_switcher") {
                    name = "soundSwitcher"
                    onClick {
                        app.sounds.playSelected()
                        app.sounds.switching()
                        when (app.music.isPlaying) {
                            true -> app.music.pause()
                            false -> app.music.play()
                        }
                    }

                    isChecked = !app.music.isPlaying
                    style = Scene2DSkin.defaultSkin.get("sound_switcher")
                }.cell(pad = 5f, width = 50f, height = 50f, align = Align.right)
            }
            table.top().left()
            table.setFillParent(true)
        }

        fun setNextLevel(nextLevel: Int) {
            levelText = "LEVEL $nextLevel"
            table.findActor<Label>("levelLabel").setText(levelText)
        }

        fun setLife(life: Int) {
            table.findActor<Label>("lifeLabel").setText(" $life")
        }

        fun setEnable(isEnable: Boolean) {
            val isTouchEnable: Touchable = if (isEnable) Touchable.enabled else Touchable.disabled
            table.findActor<Button>("backButton").touchable = isTouchEnable
            table.findActor<Button>("playSwitcher").touchable = isTouchEnable
            table.findActor<Button>("soundSwitcher").touchable = isTouchEnable
        }

        fun active(isAction: Boolean) {
            table.isVisible = isAction
        }
    }

    data class GameStageLoadMapOverlay(
            var game: Game,
            var table: Table = Table(),
            var tableBG: Table = Table(),
            var title: String = "LOAD THE MAP",
            var subTitle: String = "NOT TOO LONG",
            var isActive: Boolean = false
    ) {
        init {
            table = table {
                label(text = title, style = "yellow") {
                    setFontScale(1f)
                }.cell(expandX = true, padBottom = 30f)
                row()
                label(text = subTitle, style = "red") {
                    setFontScale(0.5f)
                }.cell(expandX = true)
            }

            val bgColor: Pixmap = Pixmap(B2D_WIDTH.toInt(), B2D_HEIGHT.toInt(), Pixmap.Format.RGB888)
            bgColor.setColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, 0.2f)
            bgColor.fill()

            tableBG = table {
                background(Image(Texture(bgColor)).drawable)
            }
            table.addActor(tableBG)
            tableBG.setFillParent(true)
            tableBG.toBack()
            table.setFillParent(true)
            table.isVisible = false
        }

        fun active(isActive: Boolean) {
            this.isActive = isActive
            table.isVisible = isActive

            if (isActive) {
                tableBG.alpha = 0.2f
            }
        }
    }

    data class GameStageReadyOverlay(
            var table: Table = Table(),
            var tableBG: Table = Table(),
            var title: String = "ARE YOU READY",
            var subTitle: String = "Tap to play",
            var isActive: Boolean = false
    ) {
        init {
            table = table {
                label(text = title, style = "yellow") {
                    setFontScale(1.2f)
                }.cell(expandX = true, padBottom = 30f)
                row()
                label(text = subTitle, style = "red") {
                    setFontScale(0.5f)
                }.cell(expandX = true)
            }

            val bgColor: Pixmap = Pixmap(B2D_WIDTH.toInt(), B2D_HEIGHT.toInt(), Pixmap.Format.RGB888)
            bgColor.setColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, 0.2f)
            bgColor.fill()

            tableBG = table {
                background(Image(Texture(bgColor)).drawable)
            }
            table.addActor(tableBG)
            tableBG.setFillParent(true)
            tableBG.toBack()
            table.setFillParent(true)
            table.isVisible = false

        }

        fun active(isActive: Boolean) {
            this.isActive = isActive
            table.isVisible = isActive

            if (isActive) {
                tableBG.alpha = 1f
                tableBG.addAction(Actions.alpha(0.4f, 1.5f))
            }
        }
    }

    data class GameStagePauseOverlay(
            var game: Game,
            var table: Table = Table(),
            var tableBG: Table = Table(),
            var title: String = "GAME PAUSE",
            var subTitle: String = "Tap to play",
            var isActive: Boolean = false
    ) {
        init {
            table = table {
                label(text = title, style = "yellow") {
                    setFontScale(1f)
                }.cell(expandX = true, padBottom = 30f)
                row()
                label(text = subTitle, style = "red") {
                    setFontScale(0.5f)
                }.cell(expandX = true)
            }

            val bgColor: Pixmap = Pixmap(B2D_WIDTH.toInt(), B2D_HEIGHT.toInt(), Pixmap.Format.RGB888)
            bgColor.setColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, 0.2f)
            bgColor.fill()

            tableBG = table {
                background(Image(Texture(bgColor)).drawable)
            }
            table.addActor(tableBG)
            tableBG.setFillParent(true)
            tableBG.toBack()
            table.setFillParent(true)
            table.isVisible = false
        }

        fun active(isActive: Boolean) {
            this.isActive = isActive
            table.isVisible = isActive

            if (isActive) {
                tableBG.alpha = 0.4f
            }
        }
    }

    data class GameStageGameOverOverlay(
            var game: Game,
            var table: Table = Table(),
            var tableBG: Table = Table(),
            var title: String = "Game Over",
            var isActive: Boolean = false
    ) {
        init {
            table = table {
                label(text = title, style = "yellow") {

                }.cell(expandX = true, padBottom = 30f)
                row()
                table {
                    imageButton("menu") {
                        name = "backToMenuButton"
                        onClick {
                            game.app.sounds.playSelected()
                            game.player.isDead = false
                            game.app.setScreen<StageMenu>()
                        }
                    }.cell(pad = 5f, width = 80f, height = 50f)
                    imageButton("retry") {
                        name = "playAgainButton"
                        onClick {
                            game.app.sounds.playSelected()
                            game.map.changeMap(game.currentLevel)
                            game.player.life = game.player.sharedPrefs.get(PrefsKey.MAX_LIFE, 5) as Int
                            game.player.isDead = false
                            game.gameStage = Game.GameStage.READY
                        }
                    }.cell(pad = 5f, width = 80f, height = 50f)
                    imageButton("next_level") {
                        name = "nextLevelButton"
                        onClick {
                            game.currentLevel += 1
                            game.map.changeMap(game.currentLevel)
                            game.hud.setNextLevel(game.currentLevel)
                        }
                    }.cell(pad = 5f, width = 80f, height = 50f)
                }
                if (REWARD_MORE_LIFE_AD) {
                    row()
                    imageTextButton(" VDO x 2 Life", "more_life") {
                        name = "MoreLifeButton"
                        label.setFontScale(0.7f)
                        isVisible = !game.app.adsHandler.isRewardMoreLife
                        onClick {
                            info { "click More Life?" }
                            game.app.sounds.playSelected()
                            game.app.adsHandler.showAdRewardVdoMoreLife()
                            game.timeTrackerStart = System.currentTimeMillis()
                        }
                    }.cell(pad = 5f, height = 50f, width = 260f, expandX = true, align = Align.center, padTop = 30f)
                }
            }

            val bgColor: Pixmap = Pixmap(B2D_WIDTH.toInt(), B2D_HEIGHT.toInt(), Pixmap.Format.RGB888)
            bgColor.setColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, 0.2f)
            bgColor.fill()

            tableBG = table {
                background(Image(Texture(bgColor)).drawable)
            }
            table.addActor(tableBG)
            tableBG.setFillParent(true)
            tableBG.toBack()
            table.setFillParent(true)
            table.isVisible = false
        }

        fun active(isActive: Boolean) {
            this.isActive = isActive
            table.isVisible = isActive
            val isNextLevelAvailable = game.currentLevel < game.sharedPrefs.get(PrefsKey.CURRENT_LEVEL) as Int
            val nextLevelBtn = table.findActor<ImageButton>("nextLevelButton")

            nextLevelBtn.touchable = if (isNextLevelAvailable) Touchable.enabled else Touchable.disabled
            nextLevelBtn.isDisabled = !isNextLevelAvailable
            nextLevelBtn.style = Scene2DSkin.defaultSkin.get("next_level")

            nextLevelBtn.isVisible = game.currentLevel < MAX_LEVEL

            val moreLifeBtn = table.findActor<ImageTextButton>("MoreLifeButton")

            moreLifeBtn.isVisible = !game.app.adsHandler.isRewardMoreLife

            if (isActive) {
                tableBG.alpha = 1f
                tableBG.addAction(Actions.alpha(0.5f, 1f))
            }
        }
    }

    data class GameStageLevelCompleteOverlay(
            var game: Game,
            var table: Table = Table(),
            var tableBG: Table = Table(),
            var title: String = COMPLETE_MESSAGE[MathUtils.random(COMPLETE_MESSAGE.size - 1)],
            var subTitle: String = "LEVEL ${game.currentLevel} CLEAR!!",
            var isActive: Boolean = false
    ) {
        init {
            table = table {
                label(text = title, style = "red") {
                    name = "titleLabel"
                    setFontScale(1f)
                }.cell(expandX = true, padBottom = 10f)
                row()
                label(text = subTitle, style = "scarlet") {
                    name = "subTitleLabel"
                    setFontScale(0.7f)
                }.cell(expandX = true, padBottom = 30f)
                row()
                table {
                    imageButton("menu") {
                        name = "backToMenuButton"
                        onClick {
                            game.app.setScreen<StageMenu>()
                        }
                    }.cell(pad = 5f, width = 80f, height = 50f)
                    imageButton("retry") {
                        name = "retryButton"
                        onClick {
                            game.currentLevel = game.currentLevel - 1
                            game.map.changeMap(game.currentLevel)
                            game.hud.setNextLevel(game.currentLevel)
                        }
                    }.cell(pad = 5f, width = 80f, height = 50f)
                    imageButton("next_level") {
                        name = "nextLevelButton"
                        onClick {
                            game.map.changeMap(game.currentLevel)
                            game.hud.setNextLevel(game.currentLevel)
                        }
                    }.cell(pad = 5f, width = 80f, height = 50f)
                }
            }

            val bgColor: Pixmap = Pixmap(B2D_WIDTH.toInt(), B2D_HEIGHT.toInt(), Pixmap.Format.RGB888)
            bgColor.setColor(Color.YELLOW.r, Color.YELLOW.g, Color.YELLOW.b, 0.2f)
            bgColor.fill()

            tableBG = table {
                background(Image(Texture(bgColor)).drawable)
            }
            table.addActor(tableBG)
            tableBG.setFillParent(true)
            tableBG.toBack()
            table.setFillParent(true)
            table.isVisible = false

        }

        fun randomCompleteMessage() {
            title = COMPLETE_MESSAGE[MathUtils.random(COMPLETE_MESSAGE.size - 1)]
            table.findActor<Label>("titleLabel").setText(title)
            table.findActor<Label>("subTitleLabel").setText("LEVEL ${game.currentLevel} CLEAR!!")
        }

        fun active(isActive: Boolean) {
            this.isActive = isActive
            table.isVisible = isActive

            table.findActor<Button>("nextLevelButton").isVisible = game.currentLevel <= MAX_LEVEL

            if (isActive) {
                tableBG.alpha = 1f
                tableBG.addAction(Actions.alpha(0.4f, 2.5f))
            }
        }
    }

    data class GameStageBackConfirmationOverlay(
            var game: Game,
            var table: Table = Table(),
            var tableBG: Table = Table(),
            var title: String = "BACK TO MENU",
            var isActive: Boolean = false
    ) {
        init {
            table = table {
                label(text = title, style = "yellow") {
                    setFontScale(1f)
                }.cell(expandX = true, pad = 50f)
                row()
                table {
                    imageButton("okay") {
                        name = "yesButton"
                        onClick {
                            game.app.sounds.playSelected()
                            game.app.setScreen<StageMenu>()
                        }
                    }.cell(pad = 5f, width = 80f, height = 50f)
                    imageButton("cancel") {
                        name = "noButton"
                        onClick {
                            game.app.sounds.playSelected()
                            game.hud.backToMenuBtn.isChecked = false
                            game.gameStage = GameStage.PAUSE
                        }
                    }.cell(pad = 5f, width = 80f, height = 50f)
                }
            }

            val bgColor: Pixmap = Pixmap(B2D_WIDTH.toInt(), B2D_HEIGHT.toInt(), Pixmap.Format.RGB888)
            bgColor.setColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, 0.2f)
            bgColor.fill()

            tableBG = table {
                background(Image(Texture(bgColor)).drawable)
            }
            table.addActor(tableBG)
            tableBG.setFillParent(true)
            tableBG.toBack()
            table.setFillParent(true)
            table.isVisible = false
        }

        fun active(isActive: Boolean) {
            this.isActive = isActive
            table.isVisible = isActive

            if (isActive) {
                tableBG.alpha = 0.4f
            }
        }
    }

    data class Tutorial(
            var game: Game,
            var table: Table = Table(),
            var currentLesson: Int = 1,
            var isActive: Boolean = false
    ) {
        init {
            val textureAtlas: TextureAtlas = game.app.assetManager.get(ATLAS_IMAGES)

            table = table {
                background(TextureRegionDrawable(textureAtlas.findRegion("tutorial1")))
            }
            table.setFillParent(true)
        }

        fun nextLesson(): Boolean {
            game.app.sounds.playSelected()
            if (currentLesson >= 2) {
                currentLesson = 1
                game.gameStage = Game.GameStage.READY
                game.show()
                table.isVisible = false
                return false
            }

            currentLesson++

            val textureAtlas: TextureAtlas = game.app.assetManager.get(ATLAS_IMAGES)

            table.addAction(Actions.sequence(
                    Actions.fadeOut(0.5f),
                    Actions.run({
                        table.background = TextureRegionDrawable(textureAtlas.findRegion("tutorial$currentLesson"))
                    }),
                    Actions.fadeIn(1f))
            )

            return true
        }

        fun active(isActive: Boolean) {
            this.isActive = isActive
            table.isVisible = isActive
        }
    }

    var world: World = app.context.inject()
    var debugRenderer: Box2DDebugRenderer = app.context.inject()
    var b2dCamera: OrthographicCamera = app.context.inject()
    var spriteBatch: SpriteBatch = app.context.inject()
    var stage: Stage = app.context.inject()
    lateinit var player: Player
    lateinit var map: GameMap
    var gameManager: GameManager
    var currentLevel: Int = 1
    val multiplexer = InputMultiplexer()
    var gameStage: GameStage = GameStage.READY
    lateinit var hud: Hud
    var loadMapOverlay: GameStageLoadMapOverlay = GameStageLoadMapOverlay(this)
    var readyOverlay: GameStageReadyOverlay = GameStageReadyOverlay()
    var pauseOverlay: GameStagePauseOverlay = GameStagePauseOverlay(this)
    var gameOverOverlay: GameStageGameOverOverlay = GameStageGameOverOverlay(this)
    var backConfirmationOverlay: GameStageBackConfirmationOverlay = GameStageBackConfirmationOverlay(this)
    var levelCompleteOverlay: GameStageLevelCompleteOverlay = GameStageLevelCompleteOverlay(this)
    var timeTrackerStart: Long = System.currentTimeMillis()
    var sharedPrefs: Prefs = app.context.inject()
    lateinit var tutorial: Tutorial
    var isTutorialDone: Boolean = false

    init {
        world.setContactListener(GameContactListener(this))
        b2dCamera.setToOrtho(false, B2D_WIDTH, B2D_HEIGHT)
        b2dCamera.position.set(B2D_WIDTH / 2f, B2D_HEIGHT / 2f, 0f)
        gameManager = GameManager(StageMode(this))
    }

    fun checkPurchasedProducts() {
        if (app.billingHandler.isPurchaseLifeUpTo10()) {
            sharedPrefs.put(PrefsKey.MAX_LIFE, 10)
            sharedPrefs.save()
        } else if (app.billingHandler.isPurchaseLifeUpTo5()) {
            sharedPrefs.put(PrefsKey.MAX_LIFE, 5)
            sharedPrefs.save()
        } else {
            sharedPrefs.put(PrefsKey.MAX_LIFE, 3)
            sharedPrefs.save()
        }
    }

    override fun show() {
        super.show()
        checkPurchasedProducts()
        hud = Hud(this.app, this)
        hud.setNextLevel(currentLevel)
        map = GameMap(this, "data/maps/stages/${currentLevel.toString().padStart(3, '0')}.tmx")
        player = Player(this)
        tutorial = Tutorial(this)
        gameManager.show()
    }

    override fun hide() {
        super.hide()
    }

    override fun pause() {
        super.pause()
        when (gameStage) {
            GameStage.LEVEL_COMPLETE -> {
            }
            GameStage.GAME_OVER -> {
            }
            GameStage.TUTORIAL -> {
            }
            else -> gameStage = GameStage.PAUSE
        }
    }

    override fun render(delta: Float) {
        super.render(delta)
        ktx.app.clearScreen(0f, 0f, 0f, 0.1f)
        gameManager.update(delta)

    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
    }

    override fun resume() {
        super.resume()
    }

    override fun dispose() {
        super.dispose()
        stage.dispose()
    }
}