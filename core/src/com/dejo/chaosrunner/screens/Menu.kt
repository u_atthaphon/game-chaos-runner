package com.dejo.chaosrunner.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.*
import com.badlogic.gdx.maps.objects.RectangleMapObject
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Align
import com.dejo.chaosrunner.ChaosRunner
import com.dejo.chaosrunner.configs.*
import ktx.actors.*
import ktx.app.KtxScreen
import ktx.collections.GdxArray
import ktx.log.info
import ktx.scene2d.*
import ktx.style.get


class Menu(var app: ChaosRunner) : KtxScreen {
    data class SettingTable(
            var app: ChaosRunner,
            var table: Table = Table()
    ) {
        init {
            table = table {
                imageButton("shop") {
                    onClick {
                        app.sounds.playSelected()
                        app.setScreen<Shop>()
                    }
                }.cell(pad = 20f, expand = true, align = Align.bottom, width = 70f, height = 70f)
                imageButton("rate") {
                    onClick {
                        app.sounds.playSelected()
                        Gdx.net.openURI("https://play.google.com/store/apps/details?id=com.dejo.chaosrunner&hl=en")
                    }
                }.cell(pad = 20f, expand = true, align = Align.bottom, width = 70f, height = 70f)
                imageButton("moregame") {
                    onClick {
                        app.sounds.playSelected()
                        Gdx.net.openURI("https://play.google.com/store/apps/developer?id=Dejo+Studio")
                        info { "moregame switch" }
                    }
                }.cell(pad = 20f, expand = true, align = Align.bottom, width = 70f, height = 70f)
                imageButton("sound_switcher") {
                    onClick {
                        app.sounds.playSelected()
                        app.sounds.switching()
                        when (app.music.isPlaying) {
                            true -> app.music.pause()
                            false -> app.music.play()
                        }
                    }
                    isChecked = !app.music.isPlaying
                    style = Scene2DSkin.defaultSkin.get("sound_switcher")
                }.cell(pad = 20f, expand = true, align = Align.bottom, width = 70f, height = 70f)
            }

            table.setFillParent(true)
        }
    }

    data class PlayerAnimation(
            var app: ChaosRunner,
            var sprite: Sprite = Sprite(),
            var walkTextureRegions: GdxArray<TextureRegion> = GdxArray<TextureRegion>(),
            var walkAnimations: Animation<TextureRegion> = Animation<TextureRegion>(1 / 5f, GdxArray<TextureRegion>()),
            var stateTime: Float = 0.toFloat(),
            var playerDirection: Direction = Direction.RIGHT
    ) {
        init {
            val textureAtlas: TextureAtlas = app.assetManager.get(ATLAS_IMAGES)

            walkTextureRegions.add(textureAtlas.findRegion("player_walk01"))
            walkTextureRegions.add(textureAtlas.findRegion("player_walk02"))
            walkAnimations = Animation<TextureRegion>(1 / 5f, walkTextureRegions)
        }

        fun update(delta: Float, spriteBatch: SpriteBatch) {
            stateTime += delta

            val currentFrame: TextureRegion = walkAnimations.getKeyFrame(stateTime, true)

            if (sprite.x <= (UNIT_SCALE * 0)) {
                playerDirection = Direction.RIGHT
            } else if (sprite.x >= (UNIT_SCALE * 383)) {
                playerDirection = Direction.LEFT
            }

            when (playerDirection) {
                Direction.LEFT -> {
                    sprite.x = sprite.x - UNIT_SCALE
                    if (!currentFrame.isFlipX) currentFrame.flip(true, false)
                }
                Direction.RIGHT -> {
                    sprite.x = sprite.x + UNIT_SCALE
                    if (currentFrame.isFlipX) currentFrame.flip(true, false)
                }
                else -> {
                }
            }

            spriteBatch.draw(currentFrame, sprite.x, sprite.y, sprite.width, sprite.height)
        }
    }

    var b2dCamera: OrthographicCamera = app.context.inject()
    lateinit var tiledMap: TiledMap
    lateinit var tiledMapRenderer: OrthogonalTiledMapRenderer
    var spriteBatch: SpriteBatch = app.context.inject()
    lateinit var playerAnimation: PlayerAnimation
    var stage: Stage = app.context.inject()
    lateinit var settingTable: SettingTable
    var tapToPlayRectangle: Rectangle = Rectangle()
    var toast: Boolean = false
    var toastTimer: Float = 0f

    init {
        b2dCamera.setToOrtho(false, B2D_WIDTH, B2D_HEIGHT)
        b2dCamera.position.set(B2D_WIDTH / 2f, B2D_HEIGHT / 2f, 0f)
    }

    override fun show() {
        super.show()
        resetToast()
        stage.clear()
        app.adsHandler.showAdFooter(false)
        settingTable = SettingTable(app)
        app.sounds.playReady()
        stage + settingTable.table
        Gdx.input.inputProcessor = null
        Gdx.input.inputProcessor = stage

        playerAnimation = PlayerAnimation(app)

        tiledMap = app.assetManager.get(TILED_MAP_MENU)
        tiledMapRenderer = OrthogonalTiledMapRenderer(tiledMap, UNIT_SCALE)
        tiledMap.layers.get("player").objects.forEach {
            when (it) {
                is TiledMapTileMapObject -> {
                    val size: Vector2 = Vector2(it.textureRegion.regionWidth / PTM, it.textureRegion.regionWidth / PTM)
                    val pos: Vector2 = Vector2(
                            (it.x * UNIT_SCALE),
                            (it.y * UNIT_SCALE)
                    )

                    playerAnimation.sprite.setSize(size.x, size.y)
                    playerAnimation.sprite.setPosition(pos.x, pos.y)
                }
            }
        }
        tiledMap.layers.get("taptoplay").objects.forEach {
            when (it) {
                is RectangleMapObject -> tapToPlayRectangle = Rectangle(
                        it.rectangle.x / PTM,
                        it.rectangle.y / PTM,
                        it.rectangle.width / PTM,
                        it.rectangle.height / PTM
                )
            }
        }
    }

    fun resetToast() {
        toast = false
        toastTimer = 0f
    }

    fun checkBackPressed(delta: Float) {
        if (toast) toastTimer += delta
        if (toast && toastTimer > 1f) resetToast()
        if (toast && Gdx.input.isKeyJustPressed(Input.Keys.BACK)) Gdx.app.exit()
        if (!toast && Gdx.input.isKeyJustPressed(Input.Keys.BACK)) {
            toast = true
            app.androidHandler.toast("Press back again to exit!")
        }
    }

    override fun render(delta: Float) {
        super.render(delta)
        checkBackPressed(delta)
        tiledMapRenderer.setView(b2dCamera)
        tiledMapRenderer.render()

        spriteBatch.projectionMatrix = b2dCamera.combined
        spriteBatch.begin()
        playerAnimation.update(delta, spriteBatch)
        spriteBatch.end()

        stage.act(delta)
        stage.draw()

        if (Gdx.input.isTouched) {
            val touchPosition: Vector3 = Vector3(Gdx.input.x.toFloat(), Gdx.input.y.toFloat(), 0f)

            b2dCamera.unproject(touchPosition)

            if (tapToPlayRectangle.contains(touchPosition.x, touchPosition.y)) app.setScreen<StageMenu>()
        }
    }

    override fun hide() {
        super.hide()
    }

    override fun dispose() {
        super.dispose()
        tiledMap.dispose()
        tiledMapRenderer.dispose()
    }
}
