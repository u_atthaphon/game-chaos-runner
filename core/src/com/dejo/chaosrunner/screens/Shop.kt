package com.dejo.chaosrunner.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Align
import com.dejo.chaosrunner.ChaosRunner
import com.dejo.chaosrunner.configs.*
import ktx.actors.onClick
import ktx.actors.plus
import ktx.app.KtxScreen
import ktx.scene2d.*
import ktx.style.get

class Shop(val app: ChaosRunner) : KtxScreen {
    var b2dCamera: OrthographicCamera = app.context.inject()
    lateinit var tiledMap: TiledMap
    lateinit var tiledMapRenderer: OrthogonalTiledMapRenderer
    val stage: Stage = app.context.inject()
    val hudTable: Table
    val menuTable: Table
    var table: Table = Table()

    init {
        b2dCamera.setToOrtho(false, B2D_WIDTH, B2D_HEIGHT)
        b2dCamera.position.set(B2D_WIDTH / 2f, B2D_HEIGHT / 2f, 0f)

        val size: Vector2 = Vector2(WIDTH - 50f, B2D_WIDTH * 5f)

        hudTable = table {
            imageButton("menu") {
                onClick {
                    app.sounds.playSelected()
                    app.setScreen<Menu>()
                }
            }.cell(padLeft = 10f, width = 50f, height = 50f, align = Align.left)
            label("SHOP", "orange")
                    .cell(
                            pad = 10f, padLeft = -20f, expandX = true,
                            height = B2D_HEIGHT * 1.9f, row = true,
                            align = Align.top
                    )
        }
        menuTable = table {
            imageTextButton("     LIFE UP TO 5", "life_up") {
                name = "lifeUpTo5Button"
                label.setFontScale(0.8f)
                touchable = if (app.billingHandler.isPurchaseLifeUpTo5() || app.billingHandler.isPurchaseLifeUpTo10()) Touchable.disabled else Touchable.enabled
                isDisabled = app.billingHandler.isPurchaseLifeUpTo5() || app.billingHandler.isPurchaseLifeUpTo10()
                style = Scene2DSkin.defaultSkin.get("life_up")
                onClick {
                    app.sounds.playSelected()
                    if (app.androidHandler.isConnectToInternet()) {
                        app.billingHandler.purchaseLifeUpTo5()
                    } else {
                        app.androidHandler.toast("Internet connection fails")
                    }
                }
            }.cell(pad = 20f, width = size.x, height = size.y, align = Align.center)
            row()
            imageTextButton("     LIFE UP TO 10", "life_up") {
                name = "lifeUpTo10Button"
                label.setFontScale(0.8f)
                touchable = if (app.billingHandler.isPurchaseLifeUpTo10()) Touchable.disabled else Touchable.enabled
                isDisabled = app.billingHandler.isPurchaseLifeUpTo10()
                style = Scene2DSkin.defaultSkin.get("life_up")
                onClick {
                    app.sounds.playSelected()
                    if (app.androidHandler.isConnectToInternet()) {
                        app.billingHandler.purchaseLifeUpTo10()
                    } else {
                        app.androidHandler.toast("Internet connection fails")
                    }
                }
            }.cell(pad = 20f, width = size.x, height = size.y, align = Align.center)
            row()
            imageTextButton("      REMOVE ADS", "remove_ads") {
                name = "removeAdsButton"
                label.setFontScale(0.8f)
                touchable = if (app.billingHandler.isPurchaseRemoveAds()) Touchable.disabled else Touchable.enabled
                isDisabled = app.billingHandler.isPurchaseRemoveAds()
                style = Scene2DSkin.defaultSkin.get("remove_ads")
                onClick {
                    app.sounds.playSelected()
                    if (app.androidHandler.isConnectToInternet()) {
                        app.billingHandler.purchaseRemoveAds()
                    } else {
                        app.androidHandler.toast("Internet connection fails")
                    }
                }
            }.cell(pad = 20f, width = size.x, height = size.y, align = Align.center)
        }

        table.setFillParent(true)
        table.add(hudTable).fillX().expandX().row()
        table.add(menuTable).fill().expand()
    }

    fun checkAvailableProduct() {
        if (app.billingHandler.isPurchaseLifeUpTo5()) {
            switchAvailableProductButton("lifeUpTo5Button", "life_up")
        }

        if (app.billingHandler.isPurchaseLifeUpTo10()) {
            switchAvailableProductButton("lifeUpTo5Button", "life_up")
            switchAvailableProductButton("lifeUpTo10Button", "life_up")
        }

        if (app.billingHandler.isPurchaseRemoveAds()) {
            switchAvailableProductButton("removeAdsButton", "remove_ads")
        }
    }

    private fun switchAvailableProductButton(buttonName: String, buttonStyle: String) {
        val btn = menuTable.findActor<ImageTextButton>(buttonName)
        btn.touchable = if (app.billingHandler.isPurchaseRemoveAds()) Touchable.disabled else Touchable.enabled
        btn.isDisabled = app.billingHandler.isPurchaseRemoveAds()
        btn.style = Scene2DSkin.defaultSkin.get(buttonStyle)
    }

    override fun show() {
        super.show()
        app.adsHandler.showAdFooter(false)
        app.sounds.playReady()
//        checkAvailableProduct()
        stage.clear()
        stage + table
        Gdx.input.inputProcessor = null
        Gdx.input.inputProcessor = stage

        tiledMap = app.assetManager.get(TILED_MAP_STAGE_MENU) // using stage menu tile map as bg
        tiledMapRenderer = OrthogonalTiledMapRenderer(tiledMap, UNIT_SCALE)
    }

    override fun render(delta: Float) {
        super.render(delta)
        checkAvailableProduct()

        if (Gdx.input.isKeyPressed(Input.Keys.BACK)) app.setScreen<Menu>()

        tiledMapRenderer.setView(b2dCamera)
        tiledMapRenderer.render()

        stage.act(delta)
        stage.draw()
    }

    override fun hide() {
        super.hide()
    }

    override fun pause() {
        super.pause()
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        stage.viewport.update(width, height, true)
    }

    override fun resume() {
        super.resume()
    }

    override fun dispose() {
        super.dispose()
        stage.dispose()
    }
}
