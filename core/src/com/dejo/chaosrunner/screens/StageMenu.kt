package com.dejo.chaosrunner.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Align
import com.dejo.chaosrunner.ChaosRunner
import com.dejo.chaosrunner.configs.*
import ktx.actors.*
import ktx.app.KtxScreen
import ktx.log.info
import ktx.scene2d.*
import ktx.style.button
import ktx.style.get

class StageMenu(val app: ChaosRunner) : KtxScreen {
    var b2dCamera: OrthographicCamera = app.context.inject()
    lateinit var tiledMap: TiledMap
    lateinit var tiledMapRenderer: OrthogonalTiledMapRenderer
    val stage: Stage = app.context.inject()
    val hudTable: Table = table {
        label("STAGE", "orange").cell(pad = 10f, height = B2D_HEIGHT * 2f)
    }
    var tableStages: Table
    var scrollPane: ScrollPane
    var table: Table = Table()
    var sharedPrefs: Prefs = app.context.inject()

    init {
        b2dCamera.setToOrtho(false, B2D_WIDTH, B2D_HEIGHT)
        b2dCamera.position.set(B2D_WIDTH / 2f, B2D_HEIGHT / 2f, 0f)

        val size: Vector2 = Vector2(B2D_WIDTH * 5f, B2D_WIDTH * 5f)
        val padding: Float = (WIDTH / PTM) * 0.5f

        tableStages = table {
            for (i in 1..MAX_LEVEL) {
                val styleName: String = if (sharedPrefs.get(PrefsKey.CURRENT_LEVEL, 1) as Int >= i) "default" else "disabled"
                val btn = button(styleName) {
                    name = i.toString()
                    label(i.toString()) {
                        setFontScale(0.8f)
                    }.cell(expand = true)
                    onClick {
                        app.sounds.playSelected()
                        val game: Game = app.getScreen<Game>()
                        game.currentLevel = i
                        app.setScreen<Game>()
                    }
                    touchable = if (sharedPrefs.get(PrefsKey.CURRENT_LEVEL, 1) as Int >= i) Touchable.enabled else Touchable.disabled
                }.cell(
                        padTop = padding, padBottom = padding, padLeft = padding, padRight = padding,
                        width = size.x, height = size.y, align = Align.center
                )

                if (i % 5 == 0) row()
            }
        }

        scrollPane = ScrollPane(tableStages)
        table.setFillParent(true)
        table.add(hudTable).fillX().expandX().row()
        table.add(scrollPane).fill().expandX().expandY()
    }

    override fun show() {
        super.show()
        app.adsHandler.showAdFooter(false)
        app.sounds.playReady()
        tableStages.children.forEach {
            if (sharedPrefs.get(PrefsKey.CURRENT_LEVEL, 1) as Int >= it.name.toInt()) {
                val btn: Button = it as Button
                btn.touchable = Touchable.enabled
                btn.style = Scene2DSkin.defaultSkin.get("default")
            }
        }
        stage.clear()
        stage + table
        Gdx.input.inputProcessor = stage

        tiledMap = app.assetManager.get(TILED_MAP_STAGE_MENU)
        tiledMapRenderer = OrthogonalTiledMapRenderer(tiledMap, UNIT_SCALE)
    }

    override fun render(delta: Float) {
        super.render(delta)

        if (Gdx.input.isKeyPressed(Input.Keys.BACK)) app.setScreen<Menu>()

        tiledMapRenderer.setView(b2dCamera)
        tiledMapRenderer.render()

        stage.act(delta)
        stage.draw()
    }

    override fun hide() {
        super.hide()
    }

    override fun pause() {
        super.pause()
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        stage.viewport.update(width, height, true)
    }

    override fun resume() {
        super.resume()
    }

    override fun dispose() {
        super.dispose()
        stage.dispose()
    }
}
