package com.dejo.chaosrunner.screens

import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TmxMapLoader
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.badlogic.gdx.utils.Timer
import com.dejo.chaosrunner.ChaosRunner
import com.dejo.chaosrunner.configs.*
import ktx.app.KtxScreen
import ktx.log.info

class Splash(val app: ChaosRunner) : KtxScreen {
    var b2dCamera: OrthographicCamera = app.context.inject()
    var tiledMap: TiledMap
    var tiledMapRenderer: OrthogonalTiledMapRenderer
    var timerFlag: Boolean = false // Workaround cause timer schedule pause and resume next screen multiple time

    init {
        b2dCamera.setToOrtho(false, B2D_WIDTH, B2D_HEIGHT)
        b2dCamera.position.set(B2D_WIDTH / 2f, B2D_HEIGHT / 2f, 0f)
        tiledMap = TmxMapLoader().load("data/maps/splash.tmx")
        tiledMapRenderer = OrthogonalTiledMapRenderer(tiledMap, UNIT_SCALE)

        app.assetManager.load(ATLAS_IMAGES, app.context.inject<TextureAtlas>().javaClass)
        app.assetManager.setLoader(TiledMap().javaClass, TmxMapLoader(InternalFileHandleResolver()))
        app.assetManager.load(TILED_MAP_MENU, TiledMap().javaClass)
        app.assetManager.load(TILED_MAP_STAGE_MENU, TiledMap().javaClass)

        app.music.isLooping = true
        app.music.volume = MUSIC_VOLUME
        app.music.play()
    }

    override fun show() {
        super.show()
        app.adsHandler.showAdFooter(false)
    }

    override fun render(delta: Float) {
        super.render(delta)
        tiledMapRenderer.setView(b2dCamera)
        tiledMapRenderer.render()

        Timer.schedule(object : Timer.Task() {
            override fun run() {
                timerFlag = true
            }
        }, 3f)

        if (timerFlag && app.assetManager.update()) {
            app.setScreen<Menu>()
        }
    }

    override fun hide() {
        super.hide()
    }

    override fun dispose() {
        super.dispose()
    }
}