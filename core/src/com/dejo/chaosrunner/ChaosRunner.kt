package com.dejo.chaosrunner

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TmxMapLoader
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.viewport.ExtendViewport
import com.dejo.chaosrunner.Handler.AndroidHandlerInterface
import com.dejo.chaosrunner.Handler.BillingHandlerInterface
import com.dejo.chaosrunner.ads.AdmobHandlerInterface
import com.dejo.chaosrunner.configs.*
import com.dejo.chaosrunner.screens.*
import ktx.app.KtxGame
import ktx.box2d.*
import ktx.inject.Context
import ktx.scene2d.Scene2DSkin


@Suppress("IMPLICIT_CAST_TO_ANY")
class ChaosRunner(var androidHandler: AndroidHandlerInterface, var adsHandler: AdmobHandlerInterface, var billingHandler: BillingHandlerInterface) : KtxGame<Screen>() {
    data class GameSounds(
            var jump: Sound = Gdx.audio.newSound(Gdx.files.internal(SOUND_JUMP)),
            var selected: Sound = Gdx.audio.newSound(Gdx.files.internal(SOUND_SELECTED)),
            var collected: Sound = Gdx.audio.newSound(Gdx.files.internal(SOUND_COLLECTED)),
            var gameOver: Sound = Gdx.audio.newSound(Gdx.files.internal(SOUND_GAME_OVER)),
            var complete: Sound = Gdx.audio.newSound(Gdx.files.internal(SOUND_COMPLETE)),
            var ready: Sound = Gdx.audio.newSound(Gdx.files.internal(SOUND_READY)),
            var hit: Sound = Gdx.audio.newSound(Gdx.files.internal(SOUND_HIT)),
            var isEnabled: Boolean = true
    ) {
        fun playJump() = if (isEnabled) jump.play(SOUND_VOLUME) else ""
        fun playSelected() = if (isEnabled) selected.play(SOUND_VOLUME) else ""
        fun playCollected() = if (isEnabled) collected.play(SOUND_VOLUME) else ""
        fun playGameOver() = if (isEnabled) gameOver.play(SOUND_VOLUME + 1) else ""
        fun playComplete() = if (isEnabled) complete.play(SOUND_VOLUME) else ""
        fun playHit() = if (isEnabled) hit.play(SOUND_VOLUME + 1) else ""
        fun playReady() = if (isEnabled) ready.play(SOUND_VOLUME) else ""
        fun switching() {
            isEnabled = !isEnabled
        }
    }


    var context: Context = Context()
    var assetManager: AssetManager = AssetManager()
    lateinit var music: Music
    lateinit var sounds: ChaosRunner.GameSounds

    override fun create() {
        music = Gdx.audio.newMusic(Gdx.files.internal(MUSIC_THEME))
        sounds = ChaosRunner.GameSounds()
        Gdx.input.isCatchBackKey = true
        register()
        addScreens()
        setScreen<Splash>()
    }

    fun register() {
        context.register {
            bindSingleton(this@ChaosRunner)
            bind { TextureAtlas() }
            bind { TiledMap() }
            bindSingleton(InternalFileHandleResolver())
            bindSingleton(TmxMapLoader())
            bindSingleton(createWorld(earthGravity))
            bindSingleton(OrthographicCamera(B2D_WIDTH, B2D_HEIGHT))
            bindSingleton(Box2DDebugRenderer())
            bindSingleton(Skin(Gdx.files.internal("data/skins/uiskin.json")))
            Scene2DSkin.defaultSkin = inject()
            bindSingleton(Stage(ExtendViewport(WIDTH, HEIGHT)))
            bindSingleton(SpriteBatch())
            bindSingleton(Splash(inject()))
            bindSingleton(Prefs())
            bindSingleton(Menu(inject()))
            bindSingleton(Shop(inject()))
            bindSingleton(StageMenu(inject()))
            bindSingleton(Game(inject()))
        }
    }


    private fun addScreens() {
        addScreen(context.inject<Splash>())
        addScreen(context.inject<Menu>())
        addScreen(context.inject<Shop>())
        addScreen(context.inject<StageMenu>())
        addScreen(context.inject<Game>())
    }

    override fun dispose() {
        context.dispose()
    }
}
