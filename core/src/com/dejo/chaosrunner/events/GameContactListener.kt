package com.dejo.chaosrunner.events

import com.dejo.chaosrunner.configs.ModelType
import com.dejo.chaosrunner.screens.Game
import com.badlogic.gdx.physics.box2d.*
import com.dejo.chaosrunner.configs.Collectible

class GameContactListener(val game: Game) : ContactListener {
    var isOneWayPlatform = false

    override fun beginContact(contact: Contact?) {
        collectCoin(contact)
        collideBegin(contact)
        jumpBegin(contact)
    }

    override fun endContact(contact: Contact?) {
        jumpEnd(contact)

        if (isOneWayPlatform) {
            contact!!.isEnabled = true
            isOneWayPlatform = false
        }
    }

    override fun preSolve(contact: Contact?, oldManifold: Manifold?) {
        oneWayPlatform(contact)
    }

    override fun postSolve(contact: Contact?, impulse: ContactImpulse?) {
    }

    fun oneWayPlatform(contact: Contact?) {
        val fixtureUserDataA = contact!!.fixtureA.userData
        val fixtureUserDataB = contact!!.fixtureB.userData
        var player: Body? = null
        var platform: Body? = null

        if (fixtureUserDataA === ModelType.PLATFORM && fixtureUserDataB === ModelType.PLAYER) {
            platform = contact!!.fixtureA.body
            player = contact!!.fixtureB.body
        }

        if (fixtureUserDataB === ModelType.PLATFORM && fixtureUserDataA === ModelType.PLAYER) {
            platform = contact!!.fixtureB.body
            player = contact!!.fixtureA.body
        }

        if (platform == null) return

        if (player!!.position.y < platform!!.position.y + 0.75f) {
            contact!!.isEnabled = false
            isOneWayPlatform = true
        }
    }

    fun collectCoin(contact: Contact?) {
        val fixtureUserDataA = contact!!.fixtureA.userData
        val fixtureUserDataB = contact!!.fixtureB.userData
        var coinBody: Body? = null
        var coin: Collectible? = null

        if (fixtureUserDataA == ModelType.PLAYER && fixtureUserDataB is Collectible) {
            coinBody = contact!!.fixtureB.body
            coin = fixtureUserDataB
        }

        if (fixtureUserDataB == ModelType.PLAYER && fixtureUserDataA is Collectible) {
            coinBody = contact!!.fixtureA.body
            coin = fixtureUserDataA
        }

        if (fixtureUserDataA == ModelType.PLAYER_FOOT && fixtureUserDataB is Collectible) {
            coinBody = contact!!.fixtureB.body
            coin = fixtureUserDataB
        }

        if (fixtureUserDataB == ModelType.PLAYER_FOOT && fixtureUserDataA is Collectible) {
            coinBody = contact!!.fixtureA.body
            coin = fixtureUserDataA
        }

        if (coinBody == null || game.player.life <= 0) return

        game.player.data.coins.add(coin)
        game.map.coinsScheduledForRemoval.add(coinBody)
        game.map.coins.removeValue(coin, true)
    }

    fun jumpBegin(contact: Contact?) {

        val fixtureUserDataA = contact!!.fixtureA.userData
        val fixtureUserDataB = contact!!.fixtureB.userData

        if (fixtureUserDataA == ModelType.PLAYER_FOOT) game.player.numFootContacts++
        if (fixtureUserDataB == ModelType.PLAYER_FOOT) game.player.numFootContacts++
    }


    fun jumpEnd(contact: Contact?) {
        val fixtureUserDataA = contact!!.fixtureA.userData
        val fixtureUserDataB = contact!!.fixtureB.userData

        if (fixtureUserDataA == ModelType.PLAYER_FOOT) game.player.numFootContacts--
        if (fixtureUserDataB == ModelType.PLAYER_FOOT) game.player.numFootContacts--
    }

    fun collideBegin(contact: Contact?) {
        val fixtureUserDataA = contact!!.fixtureA.userData
        val fixtureUserDataB = contact!!.fixtureB.userData

        if (fixtureUserDataA == ModelType.PLAYER && fixtureUserDataB == ModelType.OBSTACLE) {
            game.player.isDead = true
        } else if (fixtureUserDataA == ModelType.OBSTACLE && fixtureUserDataB == ModelType.PLAYER) {
            game.player.isDead = true
        }

    }
}
