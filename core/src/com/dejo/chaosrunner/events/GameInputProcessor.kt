package com.dejo.chaosrunner.events

import com.badlogic.gdx.Input
import com.dejo.chaosrunner.screens.Game
import ktx.app.KtxInputAdapter

class GameInputProcessor(val game: Game) : KtxInputAdapter {
    override fun keyDown(keycode: Int): Boolean {
        if (keycode == Input.Keys.BACK) game.gameStage = Game.GameStage.BACK_CONFIRMATION

        return super.keyDown(keycode)
    }

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        when (game.gameStage) {
            Game.GameStage.READY -> {
                game.gameStage = Game.GameStage.PLAY
            }
            Game.GameStage.PLAY -> {
                // check if player on ground
                if (game.player.body.linearVelocity.y == 0f) {
                    game.player.jump()
                }
            }
            Game.GameStage.PAUSE -> {
                game.hud.playSwitcherBtn.isChecked = false
                game.gameStage = Game.GameStage.PLAY
            }
            else -> {
            }
        }

        return super.touchDown(screenX, screenY, pointer, button)
    }

    //    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {

        return super.touchUp(screenX, screenY, pointer, button)
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        return super.touchDragged(screenX, screenY, pointer)
    }
}
