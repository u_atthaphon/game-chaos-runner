package com.dejo.chaosrunner.models

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.dejo.chaosrunner.configs.*
import com.dejo.chaosrunner.screens.Game
import ktx.box2d.*
import ktx.collections.GdxArray
import ktx.log.info

class Player(val game: Game) {
    var dieTexture: Texture
    val sprite: Sprite
    var body: Body
    var data = PlayerData()
    var startPositions: GdxArray<StartPosition> = game.map.startPositions
    var currentStartPosition: StartPosition
    var remainingJumpSteps: Int = 0
    var numFootContacts: Int = 0
    var isDead: Boolean = false
    var idleTextureRegions: GdxArray<TextureRegion> = GdxArray<TextureRegion>()
    var idleAnimations: Animation<TextureRegion> = Animation<TextureRegion>(1 / 2f, GdxArray<TextureRegion>())
    var walkTextureRegions: GdxArray<TextureRegion> = GdxArray<TextureRegion>()
    var walkAnimations: Animation<TextureRegion> = Animation<TextureRegion>(1 / 5f, GdxArray<TextureRegion>())
    var stateTime: Float = 0.toFloat()
    var sharedPrefs: Prefs = game.app.context.inject()
    val defaultLife: Int = 3
    var life: Int = sharedPrefs.get(PrefsKey.MAX_LIFE, defaultLife) as Int

    init {
        val textureAtlas: TextureAtlas = game.app.assetManager.get(ATLAS_IMAGES)
        dieTexture = textureAtlas.findRegion("player_die").texture
        sprite = Sprite(dieTexture)
        sprite.setSize(1f, 1f)
        currentStartPosition = startGamePosition()
        body = game.world.body {
            box(width = 0.8f, height = 1f) {
                density = 1f
                friction = 1f
                restitution = 0f
                filter.categoryBits = B2dCategory.PLAYER
                filter.maskBits = B2dCategory.BRICK
                userData = ModelType.PLAYER
            }
            position.set(startPositions[0].position)
            type = BodyDef.BodyType.DynamicBody
            fixedRotation = true
        }
        // sprite foot to detect jump
        body.box(width = 0.6f, height = 0.5f, position = Vector2(0f, -0.5f)) {
            density = 1f
            friction = 1f
            restitution = 0f
            isSensor = true
            userData = ModelType.PLAYER_FOOT
        }

        walkTextureRegions.add(textureAtlas.findRegion("player_walk01") as TextureRegion)
        walkTextureRegions.add(textureAtlas.findRegion("player_walk02") as TextureRegion)
        walkAnimations = Animation<TextureRegion>(1 / 5f, walkTextureRegions)

        idleTextureRegions.add(textureAtlas.findRegion("player_idle01") as TextureRegion)
        idleTextureRegions.add(textureAtlas.findRegion("player_idle02") as TextureRegion)

        idleAnimations = Animation<TextureRegion>(1 / 5f, idleTextureRegions)
    }

    fun reset() {
        life = sharedPrefs.get(PrefsKey.MAX_LIFE, defaultLife) as Int
        data = PlayerData()
        remainingJumpSteps = 0
        numFootContacts = 0
        stateTime = 0f
        currentStartPosition = startGamePosition()


        body = game.world.body {
            box(width = 0.8f, height = 1f) {
                density = 1f
                friction = 1f
                restitution = 0f
                filter.categoryBits = B2dCategory.PLAYER
                filter.maskBits = B2dCategory.BRICK
                userData = ModelType.PLAYER
            }
            position.set(startPositions[0].position)
            type = BodyDef.BodyType.DynamicBody
            fixedRotation = true
        }
        // sprite foot to detect jump
        body.box(width = 0.6f, height = 0.5f, position = Vector2(0f, -0.5f)) {
            density = 1f
            friction = 1f
            restitution = 0f
            isSensor = true
            userData = ModelType.PLAYER_FOOT
        }

        newPosition(currentStartPosition)
    }

    fun updateReadyStage(delta: Float) {
        stateTime += delta
        val currentFrame: TextureRegion = idleAnimations.getKeyFrame(stateTime, true)

        sprite.texture = currentFrame.texture
        sprite.setRegion(currentFrame)

        sprite.setPosition(
                body.position.x - sprite.width / 2,
                body.position.y - sprite.height / 2
        )
        setSpriteDirection(sprite)

        game.spriteBatch.draw(sprite, sprite.x, sprite.y, sprite.width, sprite.height)
    }

    fun updatePlayStage(delta: Float) {
        if (isDead) --life
        if (isDead && life <= 0) {
            game.app.sounds.playGameOver()
            body.setTransform(body.position, 0f)
            body.linearVelocity = Vector2.Zero
            body.angularVelocity = 0f
            game.gameStage = Game.GameStage.GAME_OVER

            // Check timer track to show interstitial ads for IMPRESSION_TIME_GAME_OVER_AD
            if (game.app.adsHandler.showAdGameOver(game.timeTrackerStart, IMPRESSION_TIME_GAME_OVER_AD)) {
                game.timeTrackerStart = System.currentTimeMillis()
            }

            return
        } else if (isDead) {
            game.app.sounds.playHit()
            game.gameStage = Game.GameStage.READY
            newPosition(currentStartPosition)
            isDead = false
            return
        }

        stateTime += delta

        val currentFrame: TextureRegion = walkAnimations.getKeyFrame(stateTime, true)
        sprite.texture = currentFrame.texture
        sprite.setRegion(currentFrame)
        sprite.setPosition(
                body.position.x - sprite.width / 2,
                body.position.y - sprite.height / 2
        )
        setSpriteDirection(sprite)

        game.spriteBatch.draw(sprite, sprite.x, sprite.y, sprite.width, sprite.height)

        val velocity: Vector2 = body.linearVelocity

        when (data.direction) {
            Direction.STOP -> velocity.x = 0f
            Direction.RIGHT -> velocity.x = data.speed
            Direction.LEFT -> velocity.x = -data.speed
        }

        body.linearVelocity = velocity

        // jumping
        if (numFootContacts < 1 && body.linearVelocity.y != 0f) return
        if (remainingJumpSteps > 0) {
            game.app.sounds.playJump()
            //to change velocity by jumpVelocity in one time step
            var force = body.mass * data.jumpVelocity / (1f / 30.0f) //f = mv/t
            //spread this over 3 time steps
            force /= 3.0f
            body.applyForce(Vector2(0f, force), body.worldCenter, true)
            remainingJumpSteps--
        }
    }

    fun updateGameOverStage() {
        val textureAtlas: TextureAtlas = game.app.assetManager.get(ATLAS_IMAGES)

        sprite.texture = dieTexture
        sprite.setRegion(textureAtlas.findRegion("player_die"))
        sprite.setPosition(
                body.position.x - sprite.width / 2,
                body.position.y - sprite.height / 2
        )
        setSpriteDirection(sprite)

        game.spriteBatch.draw(sprite, sprite.x, sprite.y, sprite.width, sprite.height)
    }

    fun jump() {
        remainingJumpSteps = if (remainingJumpSteps == 0) 3 else remainingJumpSteps
    }

    fun startGamePosition(): StartPosition {
        val startPosition: StartPosition = startPositions[0]
        setDirection(startPositionName = startPosition.name)

        return startPosition
    }

    fun setDirection(startPositionName: String) {
        when (startPositionName) {
            "f1left", "f2left", "f3left" -> data.direction = Direction.RIGHT
            "f1right", "f2right", "f3right" -> data.direction = Direction.LEFT
        }
    }

    fun newPositionWithRemainHighByName(nextFloorName: String) {
        var remainHigh: Float = body.position.y - currentStartPosition.position.y
        remainHigh = if (remainHigh <= 0f) 0f else remainHigh

        startPositions.forEach {
            if (it.name == nextFloorName) {
                currentStartPosition = it
                return@forEach
            }
        }

        setDirection(currentStartPosition.name)
        body.setTransform(currentStartPosition.position.x, currentStartPosition.position.y + remainHigh, 0f)
        setSpriteDirection(sprite)
    }

    fun setCurrentStartPositionByName(name: String) {
        startPositions.forEach {
            if (it.name == name) {
                info { "body x: ${body.position.x}, y: ${body.position.y}" }
                currentStartPosition = it
                return@forEach
            }
        }

        setDirection(currentStartPosition.name)
    }

    fun newPosition(startPosition: StartPosition) {
        currentStartPosition = startPosition
        body.setTransform(currentStartPosition.position, 0f)
        body.linearVelocity = Vector2.Zero
        body.angularVelocity = 0f
        setSpriteDirection(sprite)
    }

    fun setSpriteDirection(sprite: Sprite) {
        when (data.direction) {
            Direction.LEFT -> if (!sprite.isFlipX) sprite.flip(true, false)
            Direction.RIGHT -> if (sprite.isFlipX) sprite.flip(true, false)
            else -> info { "Nothing to be flip" }
        }
    }
}
