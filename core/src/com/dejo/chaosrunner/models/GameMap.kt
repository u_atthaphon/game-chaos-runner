package com.dejo.chaosrunner.models

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.maps.MapLayer
import com.badlogic.gdx.maps.MapObjects
import com.badlogic.gdx.maps.objects.*
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.badlogic.gdx.maps.tiled.TmxMapLoader
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.dejo.chaosrunner.configs.*
import com.dejo.chaosrunner.screens.Game
import ktx.box2d.body
import ktx.collections.GdxArray
import ktx.collections.gdxArrayOf
import com.badlogic.gdx.math.*
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import ktx.collections.GdxList
import ktx.collections.gdxListOf
import ktx.log.info
import com.badlogic.gdx.utils.SharedLibraryLoader.isLoaded
import com.badlogic.gdx.utils.Timer


class GameMap(val game: Game, fileName: String) {
    var tiledMap: TiledMap
    var tiledMapRenderer: OrthogonalTiledMapRenderer
    lateinit var startPositions: GdxArray<StartPosition>
    lateinit var coins: GdxArray<Collectible>
    var coinsScheduledForRemoval: GdxList<Body> = gdxListOf<Body>()


    init {
        tiledMap = TmxMapLoader().load(fileName)
        tiledMapRenderer = OrthogonalTiledMapRenderer(tiledMap, UNIT_SCALE)
        buildMap()
    }

    fun buildMap() {
        if (tiledMap.layers.get("ground") is MapLayer) {
            val groundLayer = tiledMap.layers.get("ground") as TiledMapTileLayer
            setTiles(groundLayer, ModelType.GROUND)
        }

        if (tiledMap.layers.get("platform") is MapLayer) {
            val platformLayer = tiledMap.layers.get("platform") as TiledMapTileLayer
            setTiles(platformLayer, ModelType.PLATFORM)
        }
        if (tiledMap.layers.get("brick") is MapLayer) {
//            val brickLayer = tiledMap.layers.get("brick") as TiledMapTileLayer
//            setTiles(brickLayer, ModelType.OBSTACLE)
        }

        if (tiledMap.layers.get("obstacle") is MapLayer) {
            info { "create obstacle" }
            buildObstacle(tiledMap.layers.get("obstacle").objects)
        }

        if (tiledMap.layers.get("startPosition") is MapLayer) {
            startPositions = getStartPositions(tiledMap.layers.get("startPosition").objects)
        }

        if (tiledMap.layers.get("collectible") is MapLayer) {
            coins = getCollectible(tiledMap.layers.get("collectible").objects)
        }

        if (tiledMap.layers.get("complete") is MapLayer) {
            tiledMap.layers.get("complete").isVisible = false
        }

    }

    fun buildObstacle(mapObjects: MapObjects) {
        mapObjects.forEach {
            setObject(it, ModelType.OBSTACLE)
        }
    }

    private fun getStartPositions(objectLayer: MapObjects): GdxArray<StartPosition> {
        val startPositions = gdxArrayOf<StartPosition>()

        objectLayer.forEach {
            when (it) {
                is RectangleMapObject -> {
                    val assign = StartPosition(
                            it.name,
                            Vector2(
                                    (it.rectangle.getX() * UNIT_SCALE) + 0.5f,
                                    (it.rectangle.getY() * UNIT_SCALE) + 0.5f
                            )
                    )
                    startPositions.add(assign)

                }
            }
        }

        return startPositions
    }

    private fun getCollectible(objectLayer: MapObjects): GdxArray<Collectible> {
        val coins = gdxArrayOf<Collectible>()

        objectLayer.forEach {
            when (it) {
                is TiledMapTileMapObject -> {
                    val size: Vector2 = Vector2(
                            it.properties.get("width") as Float / PTM,
                            it.properties.get("height") as Float / PTM
                    )
                    val pos: Vector2 = Vector2((it.x * UNIT_SCALE), (it.y * UNIT_SCALE))
                    val coinsTextureRegions = GdxArray<TextureRegion>()
                    val textureAtlas: TextureAtlas = game.app.assetManager.get(ATLAS_IMAGES)

                    coinsTextureRegions.add(textureAtlas.findRegion("coin01"))
                    coinsTextureRegions.add(textureAtlas.findRegion("coin02"))

                    val coinAnimations = Animation<TextureRegion>(1 / 3f, coinsTextureRegions)

                    val assign = Collectible(
                            name = "",
                            textureRegion = it.textureRegion,
                            textureRegions = coinsTextureRegions,
                            animations = coinAnimations,
                            stateTime = 0f,
                            position = pos,
                            size = size,
                            floor = it.properties["floor"] as Int
                    )
                    coins.add(assign)
                    game.world.body {
                        box(size.x, size.y) {
                            density = 0.0f
                            userData = assign
                            isSensor = true
                        }
                        position.set(
                                pos.x + ((it.textureRegion.regionWidth / PTM) / 2f),
                                pos.y + ((it.textureRegion.regionHeight / PTM))
                        )
                    }
                }
            }
        }

        return coins
    }


    private fun setObjectLayer(objectLayer: MapObjects): Unit {
        objectLayer.forEach {
            when (it) {
                is RectangleMapObject -> buildRectangle(it.rectangle)
                is PolygonMapObject -> buildPolygon(it.polygon)
                is PolylineMapObject -> buildPolyLine(it.polyline)
                is EllipseMapObject -> buildCircle(it.ellipse)
                is TiledMapTileMapObject -> buildTile(it)
            }
        }
    }

    private fun setObject(obj: Any, userData: Any): Body? {
        when (obj) {
            is RectangleMapObject -> return buildRectangle(obj.rectangle, userData)
            is PolygonMapObject -> return buildPolygon(obj.polygon, userData)
            is PolylineMapObject -> return buildPolyLine(obj.polyline, userData)
            is EllipseMapObject -> return buildCircle(obj.ellipse, userData)
            is TiledMapTileMapObject -> return buildTile(obj, userData)
        }

        return null
    }

    fun buildRectangle(rectangleObject: Rectangle, data: Any = ModelType.NONE): Body {
        val size: Vector2 = Vector2(rectangleObject.width / PTM, rectangleObject.height / PTM)
        val pos: Vector2 = Vector2(
                (rectangleObject.x * UNIT_SCALE) + ((rectangleObject.width / PTM) / 2),
                (rectangleObject.y * UNIT_SCALE) + ((rectangleObject.height / PTM) / 2)
        )
        val body = game.world.body {
            box(size.x, size.y) {
                density = 0.5f
                userData = data
            }
            position.set(pos)
        }

        return body
    }

    fun buildPolygon(polygonObject: Polygon, data: Any = ModelType.NONE): Body {
        val vertices: FloatArray = polygonObject.transformedVertices
        val worldVertices: FloatArray = FloatArray(vertices.size)

        for (i in 0..vertices.size - 1) {
            worldVertices[i] = vertices[i] / PTM
        }

        val body = game.world.body {
            polygon(worldVertices) {
                density = 0.5f
                userData = data
            }
        }

        return body
    }

    fun buildPolyLine(polyLineObject: Polyline, data: Any = ModelType.NONE): Body {
        val vertices: FloatArray = polyLineObject.transformedVertices
        val worldVertices: FloatArray = FloatArray(vertices.size)

        for (i in 0..vertices.size - 1) {
            worldVertices[i] = vertices[i] / PTM
        }

        val body = game.world.body {
            chain(worldVertices) {
                density = 0.5f
                userData = data
            }
        }

        return body
    }

    fun buildCircle(circleObject: Ellipse, data: Any = ModelType.NONE): Body {
        val radius = circleObject.width * 0.5f / PTM
        val pos: Vector2 = Vector2(
                (circleObject.x * UNIT_SCALE) + ((circleObject.width / PTM) / 2),
                (circleObject.y * UNIT_SCALE) + ((circleObject.height / PTM) / 2)
        )
        val body = game.world.body {
            circle(radius = radius) {
                restitution = 0.5f
                userData = data
            }
            position.set(pos)
        }

        return body
    }

    fun buildTile(tileObject: TiledMapTileMapObject, data: Any? = null, sensor: Boolean = false): Body {
        val size: Vector2 = Vector2(tileObject.textureRegion.regionWidth / PTM, tileObject.textureRegion.regionWidth / PTM)
        val pos: Vector2 = Vector2(
                (tileObject.x * UNIT_SCALE) + ((tileObject.textureRegion.regionWidth / PTM) / 2),
                (tileObject.y * UNIT_SCALE) + ((tileObject.textureRegion.regionWidth / PTM) / 2)
        )
        val body = game.world.body {
            box(size.x, size.y) {
                density = 0.5f
                userData = data
                isSensor = sensor
            }
            position.set(pos)
        }

        return body
    }

    private fun setTiles(layer: TiledMapTileLayer, modelType: ModelType) {
        var categoryBits: Short = 1
        var maskBits: Short = -1
        var data: Any = modelType

        when (modelType) {
            ModelType.GROUND -> {
                categoryBits = B2dCategory.GROUND
                maskBits = -1
                data = ModelType.GROUND
            }
            ModelType.OBSTACLE -> {
                categoryBits = B2dCategory.BRICK
                maskBits = B2dCategory.PLAYER
                data = ModelType.OBSTACLE
            }
            ModelType.PLATFORM -> {
                data = ModelType.PLATFORM
            }
            else -> {
            }
        }

        val tileSize = layer.tileWidth

        for (row in 0..layer.height - 1) {
            for (col in 0..layer.width - 1) {
                val cell = layer.getCell(col, row) ?: continue
                if (cell.tile == null) continue

                if (modelType == ModelType.PLATFORM) {
                    buildPlatform(row, col, tileSize)
                } else {
                    game.world.body {
                        chain(
                                Vector2(-tileSize / 2f / PTM, -tileSize / 2f / PTM),
                                Vector2(-tileSize / 2f / PTM, tileSize / 2f / PTM),
                                Vector2(tileSize / 2f / PTM, tileSize / 2f / PTM),
                                Vector2(tileSize / 2f / PTM, -tileSize / 2f / PTM),
                                Vector2(-tileSize / 2f / PTM, -tileSize / 2f / PTM)
                        ) {
                            restitution = 0f
                            filter.categoryBits = categoryBits
                            filter.maskBits = maskBits
                            userData = data
                        }
                        position.set(
                                Vector2((col + 0.5f) * tileSize / PTM, (row + 0.5f) * tileSize / PTM)
                        )
                    }
                }

            }
        }
    }

    fun buildPlatform(row: Int, col: Int, tileSize: Float): Body {
        val body = game.world.body {
            chain(
                    Vector2(-tileSize / 2f / PTM, -tileSize / 2f / PTM),
                    Vector2(-tileSize / 2f / PTM, tileSize / 2f / PTM),
                    Vector2(tileSize / 2f / PTM, tileSize / 2f / PTM),
                    Vector2(tileSize / 2f / PTM, -tileSize / 2f / PTM),
                    Vector2(-tileSize / 2f / PTM, -tileSize / 2f / PTM)
            ) {
                restitution = 0f
                filter.categoryBits = 1
                filter.maskBits = -1
                userData = ModelType.PLATFORM
            }
            position.set(
                    Vector2((col + 0.5f) * tileSize / PTM, (row + 0.5f) * tileSize / PTM)
            )
        }

        return body
    }

    fun changeMap(nextLevel: Int) {
        // set screen to load map screen
        game.gameStage = Game.GameStage.LOAD_MAP

        Gdx.app.postRunnable {
            val bodies: GdxArray<Body> = GdxArray<Body>()

            game.world.getBodies(bodies)
            bodies.forEach { game.world.destroyBody(it) }
            //Post runnable posts the below task in opengl thread

            // unload all map assets
            for (level in 1..MAX_LEVEL) {
                val levelName = "data/maps/stages/${level.toString().padStart(3, '0')}.tmx"

                if (game.app.assetManager.isLoaded(levelName)) game.app.assetManager.unload(levelName)
            }

            // load current map if done then go continue
            val nextLevelName = "data/maps/stages/${nextLevel.toString().padStart(3, '0')}.tmx"
            game.app.assetManager.load(nextLevelName, TiledMap().javaClass)
            game.app.assetManager.finishLoading()

            if (game.app.assetManager.isLoaded(nextLevelName)) {
                tiledMap = game.app.assetManager.get(nextLevelName)
                tiledMapRenderer.map.dispose()
                tiledMapRenderer.map = tiledMap
                buildMap()
                game.player.reset()
                game.app.sounds.playReady()
                game.player.isDead = false
                game.app.adsHandler.isRewardMoreLife = false

                Timer.schedule(object : Timer.Task() {
                    override fun run() {
                        if (game.gameStage != Game.GameStage.PLAY) game.gameStage = Game.GameStage.READY
                    }
                }, 0.5f)
            }
        }
    }

    fun update() {
        tiledMapRenderer.setView(game.b2dCamera)
        tiledMapRenderer.render()
    }
}