package com.dejo.chaosrunner.Handler

interface BillingHandlerInterface {
    fun isPurchaseRemoveAds(): Boolean
    fun isPurchaseLifeUpTo5(): Boolean
    fun isPurchaseLifeUpTo10(): Boolean
    fun purchaseRemoveAds()
    fun purchaseLifeUpTo5()
    fun purchaseLifeUpTo10()
    fun getRemoveAdsPrice(): String
}