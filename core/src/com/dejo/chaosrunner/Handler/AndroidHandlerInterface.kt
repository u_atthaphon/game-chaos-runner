package com.dejo.chaosrunner.Handler

interface AndroidHandlerInterface {
    fun isConnectToInternet(): Boolean
    fun toast(text: String)
}