package com.dejo.chaosrunner.managers

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.maps.MapLayer
import com.badlogic.gdx.physics.box2d.Body
import com.dejo.chaosrunner.configs.*
import com.dejo.chaosrunner.events.GameInputProcessor
import com.dejo.chaosrunner.models.Player
import com.dejo.chaosrunner.screens.Game
import ktx.actors.plus
import ktx.app.clearScreen
import ktx.collections.gdxListOf


class StageMode(var game: Game) : GameModeInterface {
    fun checkTutorial(level: Int): Boolean {
        if (level == 1) {
            game.gameStage = Game.GameStage.TUTORIAL
            return true
        }

        return false
    }

    override fun show() {
        game.app.sounds.playReady()

        if (checkTutorial(game.currentLevel) && !game.isTutorialDone) {
            game.stage.clear()
            game.stage + game.tutorial.table
            game.app.adsHandler.showAdFooter(false)
        } else {
            game.map.changeMap(game.currentLevel)
            game.stage.clear()
            game.stage + game.hud.table
            game.stage + game.loadMapOverlay.table
            game.stage + game.readyOverlay.table
            game.stage + game.pauseOverlay.table
            game.stage + game.gameOverOverlay.table
            game.stage + game.backConfirmationOverlay.table
            game.stage + game.levelCompleteOverlay.table
            game.hud.setNextLevel(game.currentLevel)
            game.multiplexer.clear()
            game.multiplexer.addProcessor(game.stage)
            game.multiplexer.addProcessor(GameInputProcessor(game))
            Gdx.input.inputProcessor = game.multiplexer
            game.app.adsHandler.showAdFooter(true)
            /*
            * game.gameStage = Game.GameStage.READY
            * will active in game.map.changeMap(game.currentLevel) after loaded map
            */
        }
    }

    override fun update(delta: Float) {
        if (game.gameStage == Game.GameStage.TUTORIAL) {
            clearScreen(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, 1f)
            OverlaySwitcher(Game.GameStage.TUTORIAL)
            game.stage.act(delta)
            game.stage.draw()
            if (Gdx.input.justTouched()) game.isTutorialDone = game.tutorial.nextLesson()

            return
        }

        if (game.gameStage == Game.GameStage.LOAD_MAP) {
            clearScreen(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, 1f)
            OverlaySwitcher(Game.GameStage.LOAD_MAP)
            game.stage.act(delta)
            game.stage.draw()

            return
        }

        clearScreen(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, 1f)
        game.b2dCamera.update()
        game.map.update()

        if (game.player.life <= 0) game.gameStage = Game.GameStage.GAME_OVER

        when (game.gameStage) {
            Game.GameStage.TUTORIAL -> {

            }
            Game.GameStage.READY -> {
                game.hud.setLife(game.player.life)
                OverlaySwitcher(Game.GameStage.READY)
                game.spriteBatch.projectionMatrix = game.b2dCamera.combined
                game.spriteBatch.begin()
                updateCoins(delta)
                game.player.updateReadyStage(delta)
                game.spriteBatch.end()

            }
            Game.GameStage.PLAY -> {
                OverlaySwitcher(Game.GameStage.PLAY)
                game.world.step(B2dStep.timeStep, B2dStep.velocityIterations, B2dStep.positionIterations)
                game.spriteBatch.projectionMatrix = game.b2dCamera.combined
                game.spriteBatch.begin()
                nextFloor(game.player)
                updateCoins(delta)
                game.player.updatePlayStage(delta)
                game.spriteBatch.end()
            }
            Game.GameStage.PAUSE -> {
                OverlaySwitcher(Game.GameStage.PAUSE)
                game.spriteBatch.projectionMatrix = game.b2dCamera.combined
                game.spriteBatch.begin()
                updateCoins(delta)
                game.player.updateReadyStage(delta)
                game.spriteBatch.end()
            }
            Game.GameStage.GAME_OVER -> {
                // Check for rewarded
                if (game.app.adsHandler.moreLife > 0) {
                    game.player.life = if (game.app.adsHandler.moreLife > 2) 2 else game.app.adsHandler.moreLife

                    game.app.adsHandler.moreLife = 0
                    game.player.newPosition(game.player.currentStartPosition)
                    game.player.isDead = false
                    game.gameStage = Game.GameStage.READY
                    return
                }

                game.hud.setLife(game.player.life)
                OverlaySwitcher(Game.GameStage.GAME_OVER)
                game.world.step(B2dStep.timeStep, B2dStep.velocityIterations, B2dStep.positionIterations)
                game.spriteBatch.projectionMatrix = game.b2dCamera.combined
                game.spriteBatch.begin()
                updateCoins(delta)
                game.player.updateGameOverStage()
                game.spriteBatch.end()
            }
            Game.GameStage.BACK_CONFIRMATION -> {
                OverlaySwitcher(Game.GameStage.BACK_CONFIRMATION)
                game.spriteBatch.projectionMatrix = game.b2dCamera.combined
                game.spriteBatch.begin()
                updateCoins(delta)
                game.player.updateReadyStage(delta)
                game.spriteBatch.end()
            }
            Game.GameStage.LEVEL_COMPLETE -> {
                OverlaySwitcher(Game.GameStage.LEVEL_COMPLETE)
                game.spriteBatch.projectionMatrix = game.b2dCamera.combined
                game.spriteBatch.begin()
                updateCoins(delta)
                game.player.updateReadyStage(delta)
                game.spriteBatch.end()
            }
        }

        game.stage.act(delta)
        game.stage.draw()
//        game.debugRenderer.render(game.world, game.b2dCamera.combined)
    }

    override fun clear() {
        game.stage.clear()
        game.multiplexer.clear()
    }

    private fun updateCoins(delta: Float) {

        game.map.coins.forEach {
            it.stateTime += delta
            val currentFrame: TextureRegion = it.animations.getKeyFrame(it.stateTime, true)

            game.spriteBatch.draw(currentFrame,
                    it.position.x, it.position.y,
                    it.size.x, it.size.y
            )
        }

        if (game.map.coinsScheduledForRemoval.size > 0) {
            game.app.sounds.playCollected()
            game.map.coinsScheduledForRemoval.forEach {
                game.world.destroyBody(it)
                game.map.coinsScheduledForRemoval = gdxListOf<Body>()
            }

            if (game.player.data.coins.size >= 3 && game.player.life != 0) {
                game.app.sounds.playComplete()
                game.levelCompleteOverlay.randomCompleteMessage()
                game.currentLevel = game.currentLevel + 1
                game.sharedPrefs.put(PrefsKey.CURRENT_LEVEL, game.currentLevel)
                game.sharedPrefs.save()

                if (game.map.tiledMap.layers.get("complete") is MapLayer) game.map.tiledMap.layers.get("complete").isVisible = true

                game.gameStage = Game.GameStage.LEVEL_COMPLETE
            }
        }
    }

    private fun nextFloor(player: Player) {
        val playerPosition = player.body.position.x

        fun setNextFloor(currentFloor: Int): Boolean {
            val left: String = "f$currentFloor${Direction.LEFT.toString().toLowerCase()}"
            val right: String = "f$currentFloor${Direction.RIGHT.toString().toLowerCase()}"
            val nextFloorLeft: String = "f" + (currentFloor + 1) + Direction.LEFT.toString().toLowerCase()
            val nextFloorRight: String = "f" + (currentFloor + 1) + Direction.RIGHT.toString().toLowerCase()
            val oppositeDirection: String = if (player.data.direction == Direction.RIGHT) right else left
            val isCollectedCoin: Boolean = player.data.coins.filter { it.floor == currentFloor }.count() > 0
            val playerCoins: Int = player.data.coins.size

            if (currentFloor <= playerCoins) return false

            if (playerPosition < 0 && isCollectedCoin) {
                player.newPositionWithRemainHighByName(nextFloorLeft)

                return true
            }

            if (playerPosition > B2D_WIDTH && isCollectedCoin) {
                player.newPositionWithRemainHighByName(nextFloorRight)

                return true
            }

            if (playerPosition < 0 || playerPosition > B2D_WIDTH) {
                player.newPositionWithRemainHighByName(oppositeDirection)

                return true
            }

            return false
        }
        for (floor in 1..3) {
            if (setNextFloor(floor)) return
        }
    }

    fun OverlaySwitcher(gameStage: Game.GameStage) {
        game.hud.setEnable(gameStage == Game.GameStage.PLAY)
        game.hud.active( gameStage != Game.GameStage.LOAD_MAP)
        game.tutorial.active(gameStage == Game.GameStage.TUTORIAL)
        game.loadMapOverlay.active(gameStage == Game.GameStage.LOAD_MAP)
        game.readyOverlay.active(gameStage == Game.GameStage.READY)
        game.pauseOverlay.active(gameStage == Game.GameStage.PAUSE)
        game.gameOverOverlay.active(gameStage == Game.GameStage.GAME_OVER)
        game.backConfirmationOverlay.active(gameStage == Game.GameStage.BACK_CONFIRMATION)
        game.levelCompleteOverlay.active(gameStage == Game.GameStage.LEVEL_COMPLETE)
    }
}