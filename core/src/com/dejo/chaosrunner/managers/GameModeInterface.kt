package com.dejo.chaosrunner.managers

interface GameModeInterface {
    fun show()
    fun update(delta: Float)
    fun clear()
}