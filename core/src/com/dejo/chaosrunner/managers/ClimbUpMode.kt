package com.dejo.chaosrunner.managers

import com.dejo.chaosrunner.screens.Game
import ktx.log.info

class ClimbUpMode(var game: Game) : GameModeInterface {
    override fun show() {
        info { "Climb up Mode show" }
    }

    override fun update(delta: Float) {
        info { "Climb up Mode update" }
    }

    override fun clear() {
        info { "clear" }
    }
}
