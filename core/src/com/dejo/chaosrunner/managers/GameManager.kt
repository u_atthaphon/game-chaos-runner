package com.dejo.chaosrunner.managers

class GameManager(var gameMode: GameModeInterface) {
    fun show() = gameMode.show()
    fun update(delta: Float) = gameMode.update(delta)
    fun clear() = gameMode.clear()

}