package com.dejo.chaosrunner.providers

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.World
import com.dejo.chaosrunner.configs.*
import ktx.inject.Context

class ChaosRunnerProvider {
    var context: Context = Context()
    fun register(): Context {
        context.register {
            bindSingleton(Box2DDebugRenderer())
            bindSingleton(World(Vector2(0f, 0f), true))
            bind{OrthographicCamera(B2D_WIDTH, B2D_HEIGHT)}
        }

        return context
    }
}