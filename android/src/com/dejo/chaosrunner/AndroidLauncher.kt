package com.dejo.chaosrunner

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.View

import com.badlogic.gdx.backends.android.AndroidApplication
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import android.widget.RelativeLayout
import com.dejo.chaosrunner.Handler.AndroidHandlerInterface
import com.dejo.chaosrunner.Handler.BillingHandlerInterface
import com.dejo.chaosrunner.ads.AdmobHandlerInterface
import com.dejo.chaosrunner.api.MyBilling
import com.dejo.chaosrunner.configs.*
import com.google.android.gms.ads.*
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import android.widget.Toast

class AndroidLauncher : AndroidApplication(),AndroidHandlerInterface, AdmobHandlerInterface, BillingHandlerInterface {
    val isDebuggable: Boolean = BuildConfig.DEBUG
    lateinit var adViewFooter: AdView
    lateinit var adViewGameOver: InterstitialAd
    lateinit var adRewardVdoMoreLife: RewardedVideoAd
    lateinit var adRequest: AdRequest.Builder
    val SHOW_ADS: Int = 1
    val HIDE_ADS: Int = 0
    lateinit var billing: MyBilling
    override var moreLife: Int = 0
    override var isRewardMoreLife: Boolean = false

    var adFooterHandler: Handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message?) {
            when (msg!!.what) {
                SHOW_ADS -> adViewFooter.visibility = View.VISIBLE
                HIDE_ADS -> adViewFooter.visibility = View.GONE
            }
        }
    }

    var adGameOverHandler: Handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message?) {
            adViewGameOver.show()
        }
    }

    var adRewardMoreLifeHandler: Handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message?) {
            if (adRewardVdoMoreLife.isLoaded) adRewardVdoMoreLife.show() else loadAdRewardVdoMoreLife()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val config = AndroidApplicationConfiguration()
        val layout = RelativeLayout(this)
        val gameView = initializeForView(ChaosRunner(this,this, this), config)

        billing = MyBilling(this)
        billing.onCreate()

        layout.addView(gameView)
        adRequest = AdRequest.Builder()

        if (ALLOWED_TEST_DEVICE && !isDebuggable) adRequest.addTestDevice(TEST_DEVICE_ID)

        adViewFooter = AdView(this)
        if (FOOTER_AD) {
            adViewFooter.adSize = AdSize.SMART_BANNER
            adViewFooter.adUnitId = if (isDebuggable) TEST_FOOTER_AD_UNIT_ID else FOOTER_AD_UNIT_ID
            adViewFooter.loadAd(adRequest.build())
            adViewFooter.adListener = object : AdListener() {
                override fun onAdClosed() {
                    super.onAdClosed()
                    gameView.requestFocus()
                }
            }

            val adParams = RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
            )
            adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
            layout.addView(adViewFooter, adParams)
            showAdFooter(false)
        }

        adViewGameOver = InterstitialAd(this)
        if (GAME_OVER_AD) {
            adViewGameOver.adUnitId = if (isDebuggable) TEST_GAME_OVER_AD_UNIT_ID else GAME_OVER_AD_UNIT_ID
            adViewGameOver.loadAd(adRequest.build())
            adViewGameOver.adListener = object : AdListener() {
                override fun onAdClosed() {
                    gameView.requestFocus()
                    // Load the next interstitial.
                    adViewGameOver.loadAd(AdRequest.Builder().build())
                }
            }
        }

        adRewardVdoMoreLife = MobileAds.getRewardedVideoAdInstance(this)
        if (REWARD_MORE_LIFE_AD) {
            adRewardVdoMoreLife.rewardedVideoAdListener = object : RewardedVideoAdListener {
                override fun onRewarded(reward: RewardItem?) {
                    moreLife = reward!!.amount
                    isRewardMoreLife = true
                    loadAdRewardVdoMoreLife()
                }

                override fun onRewardedVideoAdClosed() {
                    if (!adRewardVdoMoreLife.isLoaded) loadAdRewardVdoMoreLife()
                }

                override fun onRewardedVideoAdLeftApplication() {
                    return
                }

                override fun onRewardedVideoAdLoaded() {
                    return
                }

                override fun onRewardedVideoAdOpened() {
                    return
                }

                override fun onRewardedVideoStarted() {
                    return
                }

                override fun onRewardedVideoAdFailedToLoad(p0: Int) {
                    if (!adRewardVdoMoreLife.isLoaded) loadAdRewardVdoMoreLife()
                }
            }
            loadAdRewardVdoMoreLife()
        }

        setContentView(layout)
    }

    override fun loadAdRewardVdoMoreLife() {
        if (!isConnectToInternet()) return

        val id = if (isDebuggable) TEST_REWARD_MORE_LIFE_AD_UNIT_ID else REWARD_MORE_LIFE_AD_UNIT_ID
        adRewardVdoMoreLife.loadAd(id, adRequest.build())
    }

    override fun showAdFooter(show: Boolean) {
        if (!FOOTER_AD) return
        if (isPurchaseRemoveAds()) return

        adFooterHandler.sendEmptyMessage(if (show) SHOW_ADS else HIDE_ADS)
    }

    override fun showAdGameOver(startTime: Long, ImpressionTime: Float): Boolean {
        if (!GAME_OVER_AD) return false
        if (isPurchaseRemoveAds()) return false

        val endTime = System.currentTimeMillis()
        val DeltaTime = endTime - startTime
        val elapsedSeconds = DeltaTime / 60000.0

        if (elapsedSeconds > ImpressionTime) {
            adGameOverHandler.sendEmptyMessage(SHOW_ADS) // Nothing send actually
            return true
        }

        return false
    }

    override fun showAdRewardVdoMoreLife() {
        if (!REWARD_MORE_LIFE_AD) return
        if (isPurchaseRemoveAds()) return

        adRewardMoreLifeHandler.sendEmptyMessage(SHOW_ADS) // Nothing send actually
    }

    override fun isPurchaseRemoveAds(): Boolean {
//        info { "####billing.isAdsDisabled: ${billing.isAdsDisabled}####" }
        return billing.isAdsDisabled
    }

    override fun isPurchaseLifeUpTo5(): Boolean {
        return billing.isLifeUpTo5
    }

    override fun isPurchaseLifeUpTo10(): Boolean {
        return billing.isLifeUpTo10
    }

    override fun purchaseRemoveAds() {
        billing.purchaseRemoveAds()
    }

    override fun purchaseLifeUpTo5() {
        billing.purchaseLifeUpTo5()
    }

    override fun purchaseLifeUpTo10() {
        billing.purchaseLifeUpTo10()
    }

    override fun getRemoveAdsPrice(): String {
        return billing.getProductPrice(billing.SKU_REMOVE_ADS)
    }

    override fun isConnectToInternet(): Boolean {
        val connectivityManager: ConnectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
        if (networkInfo !is NetworkInfo || !networkInfo.isConnected || !networkInfo.isAvailable) {
            return false
        }

        return true
    }

    override fun toast(text: String) {
        handler.post {
            Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        billing.onActivityResult(requestCode, resultCode, data!!)
    }

    override fun onDestroy() {
        adRewardVdoMoreLife.destroy(this)
        super.onDestroy()
        billing.onDestroy()
    }

    override fun onResume() {
        adRewardVdoMoreLife.resume(this)
        super.onResume()
    }

    override fun onPause() {
        adRewardVdoMoreLife.pause(this)
        super.onPause()
    }
}
