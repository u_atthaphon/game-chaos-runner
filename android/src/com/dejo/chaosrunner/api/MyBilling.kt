package com.dejo.chaosrunner.api

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent

import com.dejo.chaosrunner.util.IabHelper
import com.dejo.chaosrunner.util.IabHelper.QueryInventoryFinishedListener
import com.dejo.chaosrunner.util.Purchase
import ktx.log.info


class MyBilling(val launcher: Activity) {
    // Debug tag, for logging
    val TAG: String = "ChaosRunner"

    val SKU_REMOVE_ADS: String = "com.dejo.chaosrunner.remove_ads"
    val SKU_LIFE_UP_TO_5: String = "com.dejo.chaosrunner.extendlife.5"
    val SKU_LIFE_UP_TO_10: String = "com.dejo.chaosrunner.extendlife.10"


    // (arbitrary) request code for the purchase flow
    val RC_REQUEST: Int = 10111

    var activity: Activity

    // The helper object
    lateinit var mHelper: IabHelper

    val BASE64_ENCODED_PUBLIC_KEY: String = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0mMBb3Ved76LIW3Lo2D8LtDN13F6DtPQKEUfopqVevlvYurn0oiQMgl/VgsDPLcNLHFm7V7RGV3vQTTOl4EG3Te1oA3SSn6E7nsv5h00UtmovNHZ2O1kCGaNzenhmCP07ZF5a2cBgf8hZ1I3xQODM+OpXzdG0wk++o3PZybF/wxQFL6j3+23H430Iqh8A6MHgzcVT/A8tatHJ4TUAogaCc/98vS5GAvgTHSU+ap80JQ3QWAwKuo9oAIDYf9RrVH7P9YYd6r14ATW24Noq/0Zv/LSCPA6+0wf/mSkV5KMZ/weH5LGmG2rDOMza5N4tWFhMiObQy3xkI09MAB/NXQQfwIDAQAB"
    val payload: String = "Chaos digital product"
    var isAdsDisabled: Boolean = false
    var isLifeUpTo5: Boolean = false
    var isLifeUpTo10: Boolean = false

    init {
        activity = launcher
    }

    fun onCreate() {
        // Create the helper, passing it our context and the public key to
        // verify signatures with
        info { "$TAG Creating IAB helper." }
        mHelper = IabHelper(activity, BASE64_ENCODED_PUBLIC_KEY)

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(false)

        // Start setup. This is asynchronous and the specified listener will be called once setup completes.
        info { "$TAG Starting setup." }

        mHelper.startSetup(IabHelper.OnIabSetupFinishedListener { result ->
            info { "$TAG Setup finished." }

            // Oh noes, there was a problem.
            // complain("Problem setting up in-app billing: " + result)
            if (!result.isSuccess) return@OnIabSetupFinishedListener

            // Have we been disposed off in the meantime? If so, quit.
            if (mHelper !is IabHelper) return@OnIabSetupFinishedListener

            // IAB is fully set up. Now, let's get an inventory of stuff we own.
            info { "$TAG Setup successful. Querying inventory." }
            mHelper.queryInventoryAsync(mGotInventoryListener)
        })
    }

    // Listener that's called when we finish querying the items and subscriptions we own
    var mGotInventoryListener: QueryInventoryFinishedListener = QueryInventoryFinishedListener { result, inventory ->
        info { "$TAG Query inventory finished." }

        // Have we been disposed of in the meantime? If so, quit.
        if (mHelper !is IabHelper) return@QueryInventoryFinishedListener

        // Is it a failure?
        // complain("Failed to query inventory: " + result);
        if (result.isFailure) return@QueryInventoryFinishedListener

        info { "$TAG Query inventory was successful." }

        /*
         * Check for items we own. Notice that for each purchase, we check
         * the developer payload to see if it's correct! See
         * verifyDeveloperPayload().
         */

        if (inventory.hasPurchase(SKU_REMOVE_ADS)) {
            val removeAdsPurchase = inventory.getPurchase(SKU_REMOVE_ADS)
//            mHelper.consumeAsync(removeAdsPurchase, mConsumeFinishedListener)
            isAdsDisabled = removeAdsPurchase != null && verifyDeveloperPayload(removeAdsPurchase)
        }

        if (inventory.hasPurchase(SKU_LIFE_UP_TO_5)) {
            val lifeUpTo5Purchase = inventory.getPurchase(SKU_LIFE_UP_TO_5)
//            mHelper.consumeAsync(lifeUpTo5Purchase, mConsumeFinishedListener)
            isLifeUpTo5 = lifeUpTo5Purchase != null && verifyDeveloperPayload(lifeUpTo5Purchase)
        }

        if (inventory.hasPurchase(SKU_LIFE_UP_TO_10)) {
            val lifeUpTo10Purchase = inventory.getPurchase(SKU_LIFE_UP_TO_10)
//            mHelper.consumeAsync(lifeUpTo10Purchase, mConsumeFinishedListener)
            isLifeUpTo10 = lifeUpTo10Purchase != null && verifyDeveloperPayload(lifeUpTo10Purchase)
        }
    }

    var mConsumeFinishedListener: IabHelper.OnConsumeFinishedListener = IabHelper.OnConsumeFinishedListener { purchase, result ->
        if (result.isSuccess) {
            info { "consume finished listener success" }
        } else {
            info { "consume finished listener fail" }
        }
    }

    /** Verifies the developer payload of a purchase.  */
    fun verifyDeveloperPayload(p: Purchase): Boolean {
        val payload = p.developerPayload
        /*
         * TODO: verify that the developer payload of the purchase is correct.
         * It will be the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase
         * and verifying it here might seem like a good approach, but this will
         * fail in the case where the user purchases an item on one device and
         * then uses your app on a different device, because on the other device
         * you will not have access to the random string you originally
         * generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different
         * between them, so that one user's purchase can't be replayed to
         * another user.
         *
         * 2. The payload must be such that you can verify it even when the app
         * wasn't the one who initiated the purchase flow (so that items
         * purchased by the user on one device work on other devices owned by
         * the user).
         *
         * Using your own server to store and verify developer payloads across
         * app installations is recommended.
         */
        return true
    }

    fun removeAds() {
        isAdsDisabled = true
    }

    fun lifeUpTo5() {
        isLifeUpTo5 = true
    }

    fun lifeUpTo10() {
        isLifeUpTo10 = true
    }

    // User clicked the "Remove Ads" button.
    fun purchaseRemoveAds() {
        activity.runOnUiThread {
            mHelper.launchPurchaseFlow(
                    activity, SKU_REMOVE_ADS,
                    RC_REQUEST, mPurchaseFinishedListener, payload
            )
        }
    }

    fun purchaseLifeUpTo5() {
        activity.runOnUiThread {
            mHelper.launchPurchaseFlow(
                    activity, SKU_LIFE_UP_TO_5,
                    RC_REQUEST, mPurchaseFinishedListener, payload
            )
        }
    }

    fun purchaseLifeUpTo10() {
        activity.runOnUiThread {
            mHelper.launchPurchaseFlow(
                    activity, SKU_LIFE_UP_TO_10,
                    RC_REQUEST, mPurchaseFinishedListener, payload
            )
        }
    }

    // Callback for when a purchase is finished
    var mPurchaseFinishedListener: IabHelper.OnIabPurchaseFinishedListener = IabHelper.OnIabPurchaseFinishedListener { result, purchase ->
        // if we were disposed of in the meantime, quit.
        if (mHelper !is IabHelper) return@OnIabPurchaseFinishedListener

        if (result.isFailure) {
//            complain("Error purchasing: $result")
            return@OnIabPurchaseFinishedListener
        }

        if (!verifyDeveloperPayload(purchase)) {
//            complain("Error purchasing. Authenticity verification failed.")
            return@OnIabPurchaseFinishedListener
        }

        // if true bought the premium upgrade!
        when (purchase.sku) {
            SKU_REMOVE_ADS -> removeAds()
            SKU_LIFE_UP_TO_5 -> lifeUpTo5()
            SKU_LIFE_UP_TO_10 -> lifeUpTo10()
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent): Boolean {

        if (mHelper !is IabHelper) return false

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app billing...
            return false
        } else {
            info { "onActivityResult handled by IABUtil." }
            return true
        }
    }

    // We're being destroyed. It's important to dispose of the helper here!
    fun onDestroy() {
        if (mHelper is IabHelper) mHelper.dispose()
    }

    fun complain(message: String) {
        alert("Error: $message")
    }

    fun alert(message: String) {
        activity.runOnUiThread {
            val bld = AlertDialog.Builder(activity)
            bld.setMessage(message)
            bld.setNeutralButton("OK", null)
            bld.create().show()
        }
    }

    fun getProductPrice(sku: String): String {
        if (mHelper !is IabHelper) return ""

        return mHelper.queryInventory(true, listOf(sku)).getSkuDetails(sku).price
    }
}